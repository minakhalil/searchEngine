package mainapi;

import cimain.MongoDatabase;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.min;

public class AutoComplete {

    private DB database;


    AutoComplete(DB db){
        database = db;
    }

    public List<String> autoComplete(String sub){

        List<String> words = new ArrayList<>();


        if(sub!="" || sub!=" " || sub!=null) {
            sub =sub.toLowerCase();
            sub=sub.replaceAll("[^a-z0-9']","");
            sub=sub.replaceAll("[']","\'");
            DBCollection indexerCollection = database.getCollection("collection2");

            BasicDBObject matchedFromDB = new BasicDBObject();
            matchedFromDB.put("keyword", new BasicDBObject("$regex", "^" + sub));

            System.out.println(matchedFromDB);
            List<DBObject> docs = indexerCollection.find(matchedFromDB).toArray();
            int s = min(docs.size(), 10);

            for (int i = 0; i < s; i++) {
                DBObject o = docs.get(i);

                words.add(o.get("keyword").toString());
            }
        }
        return words;
    }
}
