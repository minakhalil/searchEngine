package mainapi;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.Provider;
import java.io.IOException;


public class CORSFilter implements ContainerResponseFilter {

    public ContainerResponse filter(ContainerRequest req, ContainerResponse crunchifyContainerResponse) {

        ResponseBuilder crunchifyResponseBuilder = Response.fromResponse(crunchifyContainerResponse.getResponse());


        crunchifyResponseBuilder.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Max-Age", "151200")
                .header("Access-Control-Allow-Headers",  "Origin, X-Requested-With, Content-Type, Accept,Authorization,indentifier");

        String crunchifyRequestHeader = req.getHeaderValue("Access-Control-Request-Headers");

        if (null != crunchifyRequestHeader && !crunchifyRequestHeader.equals(null)) {
            crunchifyResponseBuilder.header("Access-Control-Allow-Headers", crunchifyRequestHeader);
        }

        crunchifyContainerResponse.setResponse(crunchifyResponseBuilder.build());
        return crunchifyContainerResponse;
    }
}