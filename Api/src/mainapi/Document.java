package mainapi;

import cimain.Bundle;
import cimain.Model_DB;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.Math.abs;
import static java.lang.Math.min;

public class Document {
    private String link;
    private List<String> keywords;
    private List<Integer> TF; //array of TFs corresponding to each keyword
    private List<Double> IDF; //array of IDFs corresponding to each keyword
    private List<Integer> positions; //the first occurence of the word in the document
    private List<String> locations;
    private double pageRank=1.0;
    private int numberOfExist; //number of words of search query in this document
    private String location;
    private double rankResult; //the result of multiplying all the numbers which is the rank of the doc
    private int numberOfOccurencesPhraseSearch;
    private int positionOfPhraseSearch;

    public int getPositionOfPhraseSearch() {
        return positionOfPhraseSearch;
    }

    public void setPositionOfPhraseSearch(int positionOfPhraseSearch) {
        this.positionOfPhraseSearch = positionOfPhraseSearch;
    }

    private String title;



    private String body;



    private String subBody;
    private int position;
    private boolean isSubscribed = false;

    public Document(String link) {
        this.link=link;
        this.keywords=new ArrayList<>();
        this.TF=new ArrayList<>();
        this.IDF=new ArrayList<>();
        this.positions=new ArrayList<>();
        this.numberOfOccurencesPhraseSearch=0;
        this.positionOfPhraseSearch=-1;
        this.locations=new ArrayList<>();







    }

    public int getNumberOfOccurencesPhraseSearch() {
        return numberOfOccurencesPhraseSearch;
    }

    public void setNumberOfOccurencesPhraseSearch(int numberOfOccurencesPhraseSearch) {
        this.numberOfOccurencesPhraseSearch = numberOfOccurencesPhraseSearch;
    }

    public void addKeyword(String keyword){
        this.keywords.add(keyword);
    }

    public void addTF(Integer tf){
        this.TF.add(tf);
    }

    public void addLocations(String s){
        this.locations.add(s);
    }

    public void addIDF(Double idf){
        this.IDF.add(idf);
    }

    public void addPosition(Integer pos){
        this.positions.add(pos);

    }


    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public double getPageRank() {
        return pageRank;
    }

    public void setPageRank(double pageRank) {
        this.pageRank = pageRank;
    }

    public double getRankResult() {
        return rankResult;
    }

    public void setRankResult(double rankResult) {
        this.rankResult = rankResult;
    }

    public Integer getpos(){
        return this.positions.get(0);
    }
    public Integer getnumexist(){
        return this.keywords.size();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getBodyString() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getPosition() {
        return positions.get(0);
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isSubscribed() {
        return isSubscribed;
    }

    public void setSubscribed(boolean subscribed) {
        isSubscribed = subscribed;
    }

    public void calculateRank(){
        double tf_idf=0;
        for(int i=0;i<this.keywords.size();i++)
        {
            tf_idf+=(this.TF.get(i)*this.IDF.get(i));
        }
        this.rankResult=this.pageRank*tf_idf;
        for(int i=0;i<this.locations.size();i++){
            if(this.locations.get(i).equals("header")){
                this.rankResult=this.rankResult*1.5;
            }else if(this.locations.get(i).equals("meta")){
                this.rankResult=this.rankResult*1.2;
            }
        }
    }

    public String getSubBody() {
        return subBody;
    }

    public void setSubBody(String subBody) {
        this.subBody = subBody;
    }

    public void setSubBody(){

        List<String> body = Arrays.asList(this.body.split("\\s+"));


        //System.out.println(this.getPosition() + " , " + body.get(this.getPosition()-1) + " , "+this.link);
        List<String> parseBody = new ArrayList<String>();
        int length = min(30,(body.size() -1 - this.getPosition()));

        if(length<0)return;

        for(int i=this.getPosition();i<(this.getPosition()+length);i++){
            parseBody.add(body.get(i));

        }


        this.subBody=String.join(" ",parseBody);


    }

    public void phraseSearchRankCalc(){
        this.rankResult=this.pageRank*this.numberOfOccurencesPhraseSearch;
    }

    public double getKeywordsNumber(){
        return this.keywords.size();
    }
}
