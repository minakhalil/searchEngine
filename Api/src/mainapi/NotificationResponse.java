package mainapi;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class NotificationResponse {



    String message;
    List<String> list;

    NotificationResponse(){

    }

    NotificationResponse(List<String> l, String m){
        list = l;
        message = m;

    }



    public void setList(List<String> list) {
        this.list = list;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getList() {
        return list;
    }
}
