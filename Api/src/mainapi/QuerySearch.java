package mainapi;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cimain.*;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import org.tartarus.snowball.ext.englishStemmer;

import static java.lang.Math.min;

public class QuerySearch {

    public String inputString;
    public String originalKeyword;
    private List<String> stopWords;
    private List<KeywordModel> keywordDetailList = new ArrayList<KeywordModel>();
    private DB database;
    private MongoDatabase mong = new MongoDatabase();

    QuerySearch(String input){
        this.inputString = input;
        stopWords=new ArrayList<String>();
        //readStopWords();
        this.database = mong.connectMongo();
    }

    private void readStopWords() {
        String line;
        BufferedReader bufferedReader = null;
        System.out.println(System.getProperty("user.dir"));
        try {
            bufferedReader = new BufferedReader(new FileReader("../stop_words.txt"));

            while((line = bufferedReader.readLine()) != null) {
                stopWords.add(line);
            }

            bufferedReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch(IOException ex) {
            ex.printStackTrace();
        }

    }

    private boolean isStopWord(String s){
        return stopWords.contains(s);
    }

    public List<String> filterWord (String input){

        List<String> wordsDummy=new ArrayList<String>();
        List<String> words=new ArrayList<String>();

        input=input.replaceAll(" @\\s+ ","");
        input=input.replaceAll("[^a-zA-Z0-9' ]"," ");

        originalKeyword = input;

        if(!input.equals(""))
            wordsDummy= Arrays.asList(input.split("\\s+"));

        englishStemmer stemmer=new englishStemmer();

        for(int k=0;k<wordsDummy.size();k++) {

            String t = wordsDummy.get(k).toLowerCase();
            if(!isStopWord(t)){

                stemmer.setCurrent(t);
                stemmer.stem();
                String ii = stemmer.getCurrent();
                words.add(ii);
            }
        }

        return words;
    }

    public  List<DBObject> searchDatabase(){

        List<String> words=filterWord(this.inputString);

        DBCollection indexerCollection = database.getCollection("collection2");

        BasicDBObject matchedFromDB =new BasicDBObject();
        matchedFromDB.put("keyword", new BasicDBObject("$in",words));


        List<DBObject> docs = indexerCollection.find(matchedFromDB).toArray();
        //System.out.println(docs);

        return docs;

    }

    public List<String>  autoComplete(String sub){

        List<String> words = new ArrayList<>();

        DBCollection indexerCollection = database.getCollection("collection2");

        BasicDBObject matchedFromDB =new BasicDBObject();
        matchedFromDB.put("keyword",new BasicDBObject("$regex","^"+sub));


        List<DBObject> docs = indexerCollection.find(matchedFromDB).toArray();
        int s = min(docs.size(),10);

        for(int i=0;i<s;i++){
            DBObject o = docs.get(i);

            words.add(o.get("keyword").toString());
        }

        return words;
    }

    public boolean  isPhraseSearch(){

        Pattern p = Pattern.compile("\\\"[\\w\\s]+\\\"");
        Matcher m = p.matcher(this.inputString);
        if (m.find()) return true;

        return false;
    }


    public void addToUserHistory(){


    }
    public static void main (String [] args){

        QuerySearch q = new QuerySearch("i'\\\"m hero\\\" boy");

//        Ranker r=new Ranker(q.searchDatabase());
//        r.fillData();
//        r.calcSearchResults();
//        r.sortSearchResults();
//        //r.print();
//        List<Document> searchResults=r.getDocuments();
//        for(int i=0;i<searchResults.size();i++)
//        {
//            System.out.print(searchResults.get(i).getLink()+"  ");
//            System.out.println(searchResults.get(i).getRankResult());
//
//        }
//        //q.autoComplete("face");

    }




}
