package mainapi;

import cimain.Bundle;
import cimain.Model_DB;
import cimain.MongoDatabase;
import cimain.Utilities;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import java.util.*;

public class Ranker {

    private Utilities util = new Utilities();
    List<Document> documents;
    List<DBObject> inputDocs;
    List<String> historyUrls;
    Map<String,Double> searchResults;
    DB database;
    double pagerank;//for test only


    public Ranker(List<DBObject> input,DB db){

        this.inputDocs=input;
        this.documents=new ArrayList<>();
        this.historyUrls=new ArrayList<>();
        database= db;
        this.pagerank=1;

    }

    public void fillData(){


        Double IDF;
        long totalDocCrawled= database.getCollection("collection1").getCount();
        Set<String> visitedUrls=new HashSet<>();//to calc number of exist
        List<String> urls;
        DBObject details;
        List<DBObject> data;

        for(int i=0;i<inputDocs.size();i++) {
            String keyword= (String) inputDocs.get(i).get("keyword");
            //TODO (@BISHO  -> foo2 ybeh)
            // division by ZERO
            IDF=Math.log10(totalDocCrawled/ ((ArrayList<String>) inputDocs.get(i).get("urls")).size());
            urls=(ArrayList<String>) inputDocs.get(i).get("urls");
            details= (DBObject) inputDocs.get(i).get("details");

            for(int k=0;k<urls.size();k++)
            {
                String s=urls.get(k);
                if(!visitedUrls.contains(s)){
                    visitedUrls.add(s);
                    s=s.replace(".","[$$]");

                    data= ((List<DBObject>) ((DBObject) details.get(s)).get("data"));
                    Integer pos = Integer.parseInt((String) data.get(0).get("position"));

                    //modification
                    String highestLocation= (String) data.get(0).get("location");
                    String tt;
                    for(int l=1;l<data.size();l++)
                    {
                        tt=(String) data.get(l).get("location");
                        if(tt.equals("header")){
                            highestLocation="header";
                        }else if(tt.equals("meta")){
                            highestLocation="meta";
                        }else{
                            highestLocation="body";
                        }
                    }


                    //////////////////////////

                    Document temp=new Document(urls.get(k));
                    temp.addKeyword(keyword);
                    temp.addTF(((Integer) ((DBObject) details.get(s)).get("repetitions")));
                    temp.addIDF(IDF);
                    temp.addPosition(pos);
                    temp.addLocations(highestLocation);

                    this.documents.add(temp);
                }else {
                    s=s.replace(".","[$$]");

                    data= ((List<DBObject>) ((DBObject) details.get(s)).get("data"));
                    Integer pos = Integer.parseInt((String) data.get(0).get("position"));

                    //modification
                    String highestLocation= (String) data.get(0).get("location");
                    String tt;
                    for(int l=1;l<data.size();l++)
                    {
                        tt=(String) data.get(l).get("location");
                        if(tt.equals("header")){
                            highestLocation="header";
                        }else if(tt.equals("meta")){
                            highestLocation="meta";
                        }else{
                            highestLocation="body";
                        }
                    }

                    addKeywordToExistingDoc(urls.get(k),keyword,((Integer) ((DBObject) details.get(s)).get("repetitions")),IDF,pos,highestLocation);

                }

            }



        }
        //System.out.println(this.documents.size());
        List<String> li = new ArrayList<>(visitedUrls);
        this.setBodyTitlePageRank(li,this.database);

    }



    private void addKeywordToExistingDoc(String link,String keyword,Integer tf,Double idf,Integer pos,String location)
    {
        for(int i=0;i<this.documents.size();i++)
        {
            if(this.documents.get(i).getLink().equals(link)){
                this.documents.get(i).addKeyword(keyword);
                this.documents.get(i).addTF(tf);
                this.documents.get(i).addIDF(idf);
                this.documents.get(i).addPosition(pos);
                this.documents.get(i).addLocations(location);
                break;
            }
        }

    }

    public void setBodyTitlePageRank(List<String> urls,DB dataBase){


        DBCollection col = dataBase.getCollection("collection1");

        BasicDBObject matchedFromDB =new BasicDBObject();
        matchedFromDB.put("url", new BasicDBObject("$in",urls));

        List<DBObject> docs = col.find(matchedFromDB).toArray();

        for(int i=0;i<docs.size();i++){

            DBObject ob = docs.get(i);

            String body = (ob.get("body") == null)?"":ob.get("body").toString();
            String title =ob.get("title").toString();
            String url =ob.get("url").toString();
            Double pr = Double.parseDouble(ob.get("pageRank").toString());

            int index = this.findDocumentByLink(url);

            if(index>=0) {
                this.documents.get(index).setTitle(title);
                this.documents.get(index).setPageRank(pr);
                this.documents.get(index).setBody(body);
                this.documents.get(index).setSubBody();

            }


        }

    }


    public void calcSearchResults(){

        for(int i=0;i<this.documents.size();i++)
        {
            this.documents.get(i).calculateRank();
        }


    }

    public void sortSearchResults(){
        this.documents.sort(new Comparator<Document>() {
            @Override
            public int compare(Document o1, Document o2) {

                if (o1.getRankResult() > o2.getRankResult()) {
                    return -1;
                } else if (o1.getRankResult() < o2.getRankResult()){
                    return 1;
                } else {
                    return 0;
                }
            }
        });
    }

    public void print(){
                for(int i=0;i<this.documents.size();i++)
        {
            System.out.print(this.documents.get(i).getLink()+"  ");
            System.out.println(this.documents.get(i).getRankResult());

        }

    }

    public void updateSubscriptions(String userSession,Subscription s) {

        List<String> mySubs = s.mySubscriptions(userSession);

        for(int i=0;i<documents.size();i++){
            if(mySubs.contains(documents.get(i).getLink())){
                documents.get(i).setSubscribed(true);
            }
        }
    }

    public int findDocumentByLink(String url){
        for(int i=0;i<this.documents.size();i++){
            if(this.documents.get(i).getLink().equals( url))
                return i;
        }
        return -1;

    }


    public List<Document> getDocuments() {
        return documents;
    }

    public List<Document> phraseSearch(String phrase){
        this.fillData();
        boolean found;
        String body;
        String title;
        int occ;
        int index;
        int phraseLength;
        List<Document> resultsOfPhraseSearch=new ArrayList<>();
        for(int i=0;i<this.documents.size();i++)
        {
            title=this.documents.get(i).getTitle();
            body=title+" "+this.documents.get(i).getBodyString();
            occ=0;
            found=false;
            index=body.indexOf(phrase);
            if(index>=0){
                this.documents.get(i).setPositionOfPhraseSearch(index);
                found=true;
            }
            phraseLength = phrase.length();
            while (index >= 0) {  // indexOf returns -1 if no match found
                occ++;
                index = body.indexOf(phrase, index + phraseLength);
            }
            if(found) {
                this.documents.get(i).setNumberOfOccurencesPhraseSearch(occ);
                this.documents.get(i).phraseSearchRankCalc();
                resultsOfPhraseSearch.add(this.documents.get(i));
            }

        }

        //user history////////////////////////////////////
        for(int i=0;i<resultsOfPhraseSearch.size();i++)
        {
            if(this.historyUrls.indexOf(resultsOfPhraseSearch.get(i).getLink())>=0){
                double temp=resultsOfPhraseSearch.get(i).getRankResult();
                temp=temp*1.1;
                resultsOfPhraseSearch.get(i).setRankResult(temp);
            }
        }
        ///////////////////////////////////////////////////////
        resultsOfPhraseSearch.sort(new Comparator<Document>() {
            @Override
            public int compare(Document o1, Document o2) {

                if (o1.getRankResult() > o2.getRankResult()) {
                    return -1;
                } else if (o1.getRankResult() < o2.getRankResult()){
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        System.out.println(resultsOfPhraseSearch);

        return resultsOfPhraseSearch;


    }

    public void setUserHistory(String user) {

        List<String> links = new ArrayList<>();

        DBCollection usersCol = database.getCollection("sessions");

        DBObject ob = new BasicDBObject();
        ob.put("sessionID", user);

        DBObject userOB = usersCol.findOne(ob);

        if (userOB == null) {
            this.historyUrls = links;
            return;
        }


        DBObject objects = (DBObject) userOB.get("history");
        if (objects != null) {
            Set<String> keys = objects.keySet();
            Map<String, Integer> map = new HashMap<String, Integer>();
            Iterator iterator = keys.iterator();

            while (iterator.hasNext()) {
                String key= ((String) iterator.next());
                int value = (int)(objects.get(key));
                map.put(util.getDotsBackToUrl(key),value);
            }

            Map<String, Integer> sortedMapDesc = sortByComparator(map);
            links = new ArrayList<>(sortedMapDesc.keySet());

        }

        this.historyUrls = links;
    }

    private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap)
    {

        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>()
        {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2)
            {

                    return o2.getValue().compareTo(o1.getValue());


            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }


    public void userHistory(){
        int size=Math.min(30,this.historyUrls.size());
        List<String> history=this.historyUrls.subList(0,size);
        for(int i=0;i<this.documents.size();i++)
        {
            if(history.indexOf(this.documents.get(i).getLink())>=0){
                double temp=this.documents.get(i).getRankResult();
                temp=temp*1.1;
                this.documents.get(i).setRankResult(temp);
            }
        }
    }

    //this function rank the docs on how many words of the search query appeared in the document
    public void numberOfQueryWords(){
        double totalWords=this.inputDocs.size();
        for(int i=0;i<this.documents.size();i++){
            double appearedindoc=this.documents.get(i).getKeywordsNumber();
            double ratio=appearedindoc/totalWords;
            double temp=this.documents.get(i).getRankResult();
            temp=temp*ratio;
            this.documents.get(i).setRankResult(temp);
        }


    }




}
