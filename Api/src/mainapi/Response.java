package mainapi;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Response<T>  {

    String message;


    Response() {

    }

    Response( String message) {
        this.message = message;
    }



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}