package mainapi;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

import static java.lang.Math.min;

@XmlRootElement
public class SearchResponse {
    public static final int RESULTS_PER_PAGE = 10;
    List<Document> results;
    int pageNumber =0;
    int resultCount =0;
    int pageCount=0;

    SearchResponse() {

    }

    SearchResponse(List<Document> r,int p) {
        results=r;
        pageNumber = p;
        resultCount=r.size();
        pageCount=r.size()/RESULTS_PER_PAGE;

    }

    public List<Document> getResults() {
        if(results.size()>0){
        int start = min(RESULTS_PER_PAGE*pageNumber,results.size());
        int end = min(start + RESULTS_PER_PAGE,results.size());

        return results.subList(start,end);
        }
        return results;

    }

    public void setResults(List<Document> results) {
        this.results = results;
    }

    public int getResultCount() {
        return resultCount;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }
}
