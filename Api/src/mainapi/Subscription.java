package mainapi;

import cimain.MongoDatabase;
import cimain.Utilities;
import com.mongodb.*;
import org.bson.Document;

import java.util.*;

public class Subscription {

    private DB database;

    private Utilities util = new Utilities();
    Subscription(DB db){
        database = db;
    }

    public  int  getCurrentCrawlerVersion() {

        DBCollection col = database.getCollection("collection4");
        return Integer.parseInt(col.findOne().get("crawelVersion").toString());

    }

    public void subscribeUser(String user,String link,boolean seen){

        DBCollection usersCol = database.getCollection("sessions");

        DBObject toBeUpdateObject =new BasicDBObject();
        toBeUpdateObject.put("sessionID", user);

        Document bDido = new Document("link", link);
        bDido.append("version",getCurrentCrawlerVersion());
        bDido.append("seen", seen);

        BasicDBObject updateQueryAppendToArray =new BasicDBObject("$set", new BasicDBObject("subs."+util.removeDotsFromUrl(link), bDido));
        usersCol.update(toBeUpdateObject,updateQueryAppendToArray,true,false);

    }

    public void unSubscribeUser(String user,String link){

        DBCollection usersCol = database.getCollection("sessions");

        DBObject toBeUpdateObject =new BasicDBObject();
        toBeUpdateObject.put("sessionID", user);



        BasicDBObject updateQueryAppendToArray =new BasicDBObject("$unset", new BasicDBObject("subs."+util.removeDotsFromUrl(link), 1));
        usersCol.update(toBeUpdateObject,updateQueryAppendToArray,true,false);

    }

    public int getLinkLatestVerison(String link) {
        DBCollection subsCol = database.getCollection("subscriptions");
        DBObject ob = subsCol.findOne(new BasicDBObject("url",link));
        if(ob ==null) return -1;

        return Integer.parseInt(ob.get("version").toString());

    }

    public List<String> getNotifications(String user){

        List<String> links = new ArrayList<>();

        DBCollection usersCol = database.getCollection("sessions");

        DBObject ob =new BasicDBObject();
        ob.put("sessionID", user);

        DBObject userOB = usersCol.findOne(ob);

        if(userOB == null) return links;


        DBObject objects = (DBObject) userOB.get("subs");
        if(objects !=null) {
            Set<String> keys = objects.keySet();

            Iterator iterator = keys.iterator();
            BasicDBObject updateQueryAppendToArray = new BasicDBObject();
            while (iterator.hasNext()) {
                DBObject internalOB = (DBObject) objects.get((String) iterator.next());

                int subsver = getLinkLatestVerison(util.getDotsBackToUrl(internalOB.get("link").toString()));
                int myVersion = Integer.parseInt(internalOB.get("version").toString());
                boolean seen = internalOB.get("seen").toString() == "true" ? true : false;

                if (myVersion < subsver || !seen) {

                    links.add(util.getDotsBackToUrl(internalOB.get("link").toString()));
                    updateQueryAppendToArray.append("$set", new BasicDBObject("subs." + util.removeDotsFromUrl(internalOB.get("link").toString()) + ".seen", false));
                    if (subsver != -1)
                        updateQueryAppendToArray.append("$set", new BasicDBObject("subs." + util.removeDotsFromUrl(internalOB.get("link").toString()) + ".version", subsver));

                }
            }

        if(links.size()>0)
        usersCol.update(ob,updateQueryAppendToArray);
        }
        return links;
    }

    public List<String> mySubscriptions(String user){

        List<String> links = new ArrayList<>();

        DBCollection usersCol = database.getCollection("sessions");

        DBObject ob =new BasicDBObject();
        ob.put("sessionID", user);

        DBObject userOB = usersCol.findOne(ob);
        if(userOB == null) return links;


        DBObject objects = ((DBObject) userOB.get("subs"));

        if(objects !=null) {
            Set<String> keys = objects.keySet();
            Iterator iterator = keys.iterator();

            while (iterator.hasNext()) {
                DBObject internalOB = (DBObject) objects.get((String) iterator.next());
                links.add(internalOB.get("link").toString());
            }
        }
        return links;
    }

    public void addToUserHistory(String user,List<DBObject> list,boolean x){


        List<String> urls = new ArrayList<String>();

        for(int i=0;i<list.size();i++)
            urls.addAll((ArrayList<String>) list.get(i).get("urls"));

        Set<String>uniqueUrls = new HashSet<String>(urls);
        List<String> uniqueUrlsList = new ArrayList<>(uniqueUrls);


        DBCollection usersCol = database.getCollection("sessions");

        DBObject toBeUpdateObject =new BasicDBObject();
        toBeUpdateObject.put("sessionID", user);


        BasicDBObject updateQueryAppendToArray =new BasicDBObject();
        BasicDBObject toBeInc = new BasicDBObject();

        for(int i=0;i<uniqueUrlsList.size();i++) {
            toBeInc.append("history." + util.removeDotsFromUrl(uniqueUrlsList.get(i)), 1);
        }

        updateQueryAppendToArray.append("$inc", toBeInc);


        if(user!=null&&uniqueUrlsList.size()>0)
        usersCol.update(toBeUpdateObject,updateQueryAppendToArray,true,false);

    }

    public void addToUserHistory(String user,List<mainapi.Document> list){


        List<String> urls = new ArrayList<String>();

        for(int i=0;i<list.size();i++)
            urls.add(list.get(i).getLink());

        Set<String>uniqueUrls = new HashSet<String>(urls);
        List<String> uniqueUrlsList = new ArrayList<>(uniqueUrls);


        DBCollection usersCol = database.getCollection("sessions");

        DBObject toBeUpdateObject =new BasicDBObject();
        toBeUpdateObject.put("sessionID", user);


        BasicDBObject updateQueryAppendToArray =new BasicDBObject();
        BasicDBObject toBeInc = new BasicDBObject();

        for(int i=0;i<uniqueUrlsList.size();i++) {
            toBeInc.append("history." + util.removeDotsFromUrl(uniqueUrlsList.get(i)), 1);
        }

        if(uniqueUrlsList.size()>0)
        updateQueryAppendToArray.append("$inc", toBeInc);


        if(user!=null&&uniqueUrlsList.size()>0)
            usersCol.update(toBeUpdateObject,updateQueryAppendToArray,true,false);

    }
}
