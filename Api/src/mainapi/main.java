package mainapi;
import javax.print.Doc;
import javax.ws.rs.*;
import java.util.ArrayList;
import java.util.List;

import cimain.*;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.sun.deploy.util.StringUtils;


@Path("/api")
public class main {

    Model_DB md = new Model_DB();
    MongoDatabase mong = new MongoDatabase();
    DB dataBase = mong.connectMongo();

    Subscription sub = new Subscription(dataBase);
    AutoComplete ac = new AutoComplete(dataBase);

    @GET
    @Path("/search")
    @Produces("application/json")
    public SearchResponse search(@HeaderParam("identifier") String sessionID,@QueryParam("q") String query,@QueryParam("page") int pageNumber) {
        System.out.println("search "+sessionID + " - " + query);
        QuerySearch q = new QuerySearch(query);
        List<DBObject> docs = q.searchDatabase();
        Ranker r =new Ranker(docs,dataBase);
        r.setUserHistory(sessionID);
        List<Document> list;

        if(q.isPhraseSearch()) {

            list = r.phraseSearch(q.originalKeyword);
            sub.addToUserHistory(sessionID,list);

        }

        else {

            sub.addToUserHistory(sessionID,docs,true);
            r.fillData();
            r.calcSearchResults();
            r.numberOfQueryWords();
            r.userHistory();
            r.sortSearchResults();
            r.updateSubscriptions(sessionID, sub);
            list=r.getDocuments();
        }



        return new SearchResponse(list,pageNumber);
    }

    @GET
    @Path("/subscribe")
    @Produces("application/json")
    public Response subscribe(@HeaderParam("identifier") String sessionID,@QueryParam("l") String link) {
        System.out.println("subscribe "+sessionID + " - " + link);
        if(sessionID == null || link == null)
            return new Response("missing data");
        sub.subscribeUser(sessionID,link,true);
        return new Response("done");
    }

    @GET
    @Path("/unsubscribe")
    @Produces("application/json")
    public Response unSubscribe(@HeaderParam("identifier") String sessionID,@QueryParam("l") String link) {
        System.out.println("unsubscribe "+sessionID + " - " + link);
        if(sessionID == null || link == null)
            return new Response("missing data");
        sub.unSubscribeUser(sessionID,link);
        return new Response("done");
    }

    @GET
    @Path("/getnotifications")
    @Produces("application/json")
    public NotificationResponse getMyNotifications(@HeaderParam("identifier") String sessionID) {
        System.out.println("getnotifications "+sessionID);
        if(sessionID == null)
            return new NotificationResponse(null,"missing data");
        List<String> list= sub.getNotifications(sessionID);

        return new NotificationResponse(list,"done");
    }

    @GET
    @Path("/autocomplete")
    @Produces("application/json")
    public NotificationResponse AutoComplete(@QueryParam("w") String word) {

        System.out.println("Autocomplete "+word);
        if(word == null)
            return new NotificationResponse(null,"missing data");
        List<String> list= ac.autoComplete(word);
        return new NotificationResponse(list,"done");
    }
}
