package cimain;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Brenda
 */
public class Bundle {

    private String url;
    private List<String> childs;

    /*  @MKA
     * IMPORTANT
     * make parents LIST not STRING
     * setter/getters modified
     *
     * */
    private List<String> parents;
    private String robots;
    private String description ;
    private String title;
    private String header;
    private String body;
    private int priority;
    private boolean indexed;
    private String hash ;
    private double pageRank;
    public double getPageRank(){return pageRank;}
    public void setPageRank(double num){pageRank=num;}
    public String getHash()
    {
        return hash;
    }
    public void setHash(String s)
    {
        hash=s;
    }


    public String getUrl() {
        return url;
    }

    public int getPriority() {
        return priority;
    }
    public void setPriority(int p)
    {
        priority=p;
    }

    public List<String> getChilds() {
        return childs;
    }

    public List<String> getParents() {
        return parents;
    }

    public String getRobots() {
        return robots;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public String getHeader() {
        return header;
    }

    public String getBody() {
        return body;
    }

    public Bundle()
    {
        url="";
        robots="";
        description="";
        title="";
        header="";
        body="";
        indexed=false;
        childs=new ArrayList<>();
        parents= new ArrayList<>();
        priority=0;
        pageRank=1;

    }
    public void setChild(List<String> ch)
    {
        childs.addAll(ch);

    }
    public boolean getIndexed()
    {
        return indexed;
    }
    public void setIndexed(boolean indexed)
    {
        this.indexed=indexed;
    }
    public  String getChild(int i)
    {
        return childs.get(i);
    }
    public int getChildCount()
    {
        return childs.size();
    }
    public void setUrl(String url)
    {
        this.url=url;
    }
    public void setParent(List<String> pr)
    {
        for(int i=0 ; i<pr.size();i++)
        {
            parents.add(pr.get(i));
        }

    }
    public void setRobots(String robots)
    {
        this.robots=robots;
    }
    public void setDescription(String desc)
    {
        this.description=desc;
    }
    public void setTitle(String title)
    {
        this.title=title;
    }
    public void setHeader(String header)
    {
        this.header=header;
    }
    public void setBody(String body)
    {
        this.body=body;
    }


}