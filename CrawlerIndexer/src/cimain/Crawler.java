package cimain;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Brenda
 */
public class Crawler implements Runnable {


    private List<String> urlNotVisited;
    private int stopCreatriaNumber;
    private PageVisitedByCrawler pageVisited;
    private boolean recrawled=false;
    private Model_DB db;
    private boolean first;
    private String seed;



    Crawler(String name,int stopCreatria,Model_DB db,int crawlingType)
    {
        urlNotVisited = new LinkedList<String>();
        stopCreatriaNumber=stopCreatria;
        // urlNotVisited.add(name);
        seed=name;
        //this.pageVisited=pageVisited;
        this.db=db;
        if(crawlingType==-1)
        {
            first=true;
            //first time , and it's not interrupted
        }
        else
        {
            first=false;
        }

    }
    Crawler(int stopCreatria,Model_DB db,int crawlingType)
    {
        stopCreatriaNumber=stopCreatria;
        urlNotVisited=new ArrayList<>();
        this.db=db;
        if(crawlingType==-1)
        {
            first=true;
            //first time , and it's not interrupted
        }
        else
        {
            first=false;
        }
        if(crawlingType==1)
            recrawled=true;
    }





    @Override
    public void run() {
        System.out.println("hello from Crawler Thread");


        while((stopCreatriaNumber>0 && recrawled==false) || (stopCreatriaNumber>0 && recrawled==true && db.getUrlReCrawledCount()>0 ) )
        {
            if(recrawled==false)
            {
                String [] url= new String[2];
                //it's supposed to be while we didn't reach the stopping creatria
                if(first==false)
                {
                    url=nextUrl();
                }
                else
                {
                    url[0]=seed;
                    url[1]=seed;

                    first=false;
                }

                boolean result=createSpider(url);
                if(result)
                {
                    stopCreatriaNumber--;
                    //System.out.println("***total unvisited links*** " + (urlNotVisited.size()));
                }
            }
            else
            {

                String [] url= new String[2];
                url=nextUrlRecrawel();

                boolean result=createSpider(url);
                if(result)
                {
                    stopCreatriaNumber--;
                    //System.out.println("***total unvisited links*** " + (urlNotVisited.size()));
                }





            }


        }



    }

    private String [] nextUrlRecrawel()
    {
        String [] nextUrl=db.getURLReCrawling();
        return nextUrl;

    }

    private String[] nextUrl()
    {
        String [] nextUrl=db.getUnvisitedUrl();

        while(nextUrl==null ){
            nextUrl=db.getUnvisitedUrl();
        }
        while(nextUrl!=null )
        {
            //nextUrl = urlNotVisited.remove(0);
            if(db.isVisited(nextUrl[0])==false)
            {
                //if it's not already visited

                return nextUrl;
            }
            db.saveNewParent(nextUrl[1],nextUrl[0]);
            nextUrl=db.getUnvisitedUrl();
        }
        return null;
    }

    private boolean createSpider(String [] url)
    {
        System.out.println(Thread.currentThread().getName() + ", still to go: " + this.stopCreatriaNumber);
        Spider spider= new Spider(url[0]);

        //if it's during crawling or it's recrawling but a new seed

        if(spider.success)
        {
            Bundle data =spider.getData();
            if(recrawled==true && db.isVisited(url[0]))
            {
                Bundle B=db.getBundle(url[0]);
                B.setUrl(url[0]);
                String hash= B.getHash();
                if(hash.equals(data.getHash()))
                {
                    //page didn't change

                    //actually will increase in term of numbers
                    db.increasePriority(url[0]);
                    db.incrementCounterOfStoppingCreatria();
                    return true;
                }

                List<String> ch =B.getChilds();

                List<String[]> childTemp = new ArrayList<>();

                for(int i=0; i<data.getChildCount();i++)
                {

                    if(!(ch.contains(data.getChild(i))))
                    {
                        String temp []= new String[2];
                        temp[0]=data.getChild(i);
                        temp[1]=url[0];
                        childTemp.add(temp);

                    }

                }
                B.setHash(data.getHash());
                B.setBody(data.getBody());
                B.setHeader(data.getHeader());
                B.setTitle(data.getTitle());
                B.setDescription(data.getDescription());
                B.setRobots(data.getRobots());
                //B.setPageRank(data.getPageRank());
                db.saveUnvisitedUrl(childTemp);
                B.setIndexed(false);
                db.saveBundle2(B);
                //db.increasePriority(url[0]);
                db.decreasePriority(url[0]);
                db.incrementCounterOfStoppingCreatria();
                db.insertOrUpdateInSubscribtion(url[0]);
                return true;

            }

            List<String[]> childTemp = new ArrayList<>();
            for(int i=0; i<data.getChildCount();i++)
            {
                String temp []= new String[2];
                temp[0]=data.getChild(i);
                temp[1]=url[0];
                childTemp.add(temp);
            }

            db.saveUnvisitedUrl(childTemp);
            //pageVisited.makeVisited(url[0]);
            List<String>pr;
            pr = new ArrayList<>();
            pr.add(url[1]);
            data.setParent(pr);
            boolean result=db.saveBundle(data);
            if(result)
            {
                db.incrementCounterOfStoppingCreatria();
                return true;
            }
            else
                return false;


        }
        else {
            //TODO
            //if spider failed
            return false;
        }





    }




}

