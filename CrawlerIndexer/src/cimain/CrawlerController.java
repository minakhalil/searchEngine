package cimain;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Brenda
 */
public class CrawlerController {

    private List<String> myList;
    private Crawler[] threadArray ;
    private PageVisitedByCrawler pageVisited ;
    private Model_DB db;
    private int numOfThreads;
    private  final int MAX_PAGES = 5000;
    private boolean crawled=false;
    private int firstTime;

//private boolean firstTime;// to indicate if he is returning from an interrupt or it's the first time

    public void main ()
    {
        System.out.println("hello from controllerCrawler");
        intialization();
        //if not craweled , create thread and go on the normal flow
        if(firstTime<=0)
        {
            try
            {
                createThreads();
            }
            catch (InterruptedException ex)
            {
                System.out.println("can't create threads");
            }
        }

        for(int i=0 ; i<4;i++)
        {
            calculatePageRank();
            try {
                reCrawel();
                int test=0;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }






    }
    private void intialization ()
    {
        String line ="";
        db=new Model_DB();
        myList = new ArrayList<>();
        pageVisited= new PageVisitedByCrawler(db);

        try
        {
            FileReader fileReader = new FileReader("input.txt");

            BufferedReader bufferedReader =new BufferedReader(fileReader);


            line = bufferedReader.readLine();
            if(line==null)
            {
                firstTime=-1;
                System.out.println("enter number of threads");
                Scanner in = new Scanner(System.in);
                numOfThreads = in.nextInt();
                // we will make the user enter seeds equivalent to number of threads
                System.out.println("enter a seed");
                FileReader fileReaderSeed = new FileReader("seed.txt");
                BufferedReader bufferedReaderSeed =new BufferedReader(fileReaderSeed);
                for(int i=0 ; i<numOfThreads ;i++)
                {


                    String input=bufferedReaderSeed.readLine();
                    myList.add(input);
                }
                List<String[]> tempList = new ArrayList<>();
                String input=bufferedReaderSeed.readLine();
                if(input!=null)
                {

                    String temp []= new String[2];
                    temp[0]=input;
                    temp[1]=input;
                    tempList.add(temp);
                }

                while(input!=null)
                {
                    input=bufferedReaderSeed.readLine();
                    if(input!=null)
                    {
                        String temp []= new String[2];
                        temp[0]=input;
                        temp[1]=input;
                        tempList.add(temp);
                    }
                }
                if(tempList.size()>0)
                {
                    db.saveUnvisitedUrl(tempList);
                }


                bufferedReader.close();

                try {
                    FileWriter fileWriter =new FileWriter("input.txt");
                    BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                    bufferedWriter.write("firsttime");
                    bufferedWriter.newLine();

                    String dummy="";
                    dummy=dummy+numOfThreads;
                    bufferedWriter.write(dummy);

                    bufferedWriter.close();
                }
                catch(IOException ex) {
                    System.out.println( "Error writing to file '");

                }


            }
            else

            {
                //the crawler/recrawler got interrupted

                line = bufferedReader.readLine();
                numOfThreads=Integer.parseInt(line);
                line= bufferedReader.readLine();

                if(line!=null)
                {

                    line= bufferedReader.readLine();
                    if(line!=null)
                    {

                        firstTime=2;
                        //cutted during recrawling
                        if(db.getUrlReCrawledCount()==0)
                        {
                            firstTime=1;
                        }

                    }
                    else
                    {
                        firstTime=1;
                        //recrawling for first time
                    }




                }

                else
                {
                    firstTime=0;

                }

                bufferedReader.close();



            }


        }
        catch(FileNotFoundException ex) {
            System.out.println( "Unable to open file '");
        }
        catch(IOException ex) {
            System.out.println("Error reading file '" );

        }




    }
    private void createThreads() throws InterruptedException
    {
        threadArray = new Crawler[numOfThreads]; //dynamic array of crawler
        Thread[] temp= new Thread [numOfThreads] ;
        int target=db.getStoppingCreatria();
        int stopCreatria=(MAX_PAGES-target)/numOfThreads;
        int stopCreatria2=((MAX_PAGES-target)/numOfThreads)+((MAX_PAGES-target)%numOfThreads);
        for(int i=0 ; i<numOfThreads;i++)
        {
            if(firstTime==-1)
            {
                if(i==0)
                {
                    threadArray[i] = new Crawler(myList.get(i),stopCreatria2,db,firstTime);
                }
                else
                {
                    threadArray[i] = new Crawler(myList.get(i),stopCreatria,db,firstTime);
                }
            }
            else if (firstTime==0)
            {
                //crawler got interrupted
                if(i==0)
                {
                    threadArray[i] = new Crawler(stopCreatria2,db,firstTime);
                }
                else
                {
                    threadArray[i] = new Crawler(stopCreatria,db,firstTime);
                }
            }



            temp[i]= new Thread(threadArray[i]);
            //temp = new Thread(threadArray[i]).start();
            temp[i].start();


        }

        for(int i=0 ; i<numOfThreads;i++)
        {
            System.out.println("---------------------------------------------------------" + i + "joined");
            temp[i].join();
        }
        if(db.getStoppingCreatria()>=MAX_PAGES)
        {
            db.clearStoppingCritria();
            try {
                FileWriter fileWriter =new FileWriter("input.txt",true);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                bufferedWriter.newLine();
                bufferedWriter.write("crawled");
                bufferedWriter.close();



            }
            catch(IOException ex) {
                System.out.println( "Error writing to file '");

            }
            db.incCurrentCrawelVersion();
        }




    }
    private void reCrawel() throws InterruptedException
    {
        List<String[]> listUrl;

        // get priority state from database and inc
        if(firstTime<=1)
        {
            int priority = db.getPriorityState();
            int unvisitedCount = db.getUnvisitedCount();
            listUrl = db.getUrlWithPriority(0);
            int count=0;



            if (priority == 0) {
                //0, unvisited, 1, 2

                if (unvisitedCount >= (MAX_PAGES - listUrl.size()))
                {
                    count=MAX_PAGES - listUrl.size();
                    for (int i = 0; i < count; i++) {
                        String[] url = db.getUnvisitedUrl();
                        listUrl.add(url);

                    }
                }
                else {

                    for (int i = 0; i < unvisitedCount; i++) {


                        String[] url = db.getUnvisitedUrl();
                        listUrl.add(url);

                    }

                    List<String[]> listUrl2 = db.getUrlWithPriority(1);
                    count=MAX_PAGES - listUrl.size();

                    if (listUrl2.size() >= count)
                    {
                        for (int i = 0; i < count; i++)
                        {
                            listUrl.add(listUrl2.get(i));
                        }
                    }
                    else {

                        for (int i = 0; i < listUrl2.size(); i++)
                        {
                            listUrl.add(listUrl2.get(i));
                        }

                        List<String[]> listUrl3 = db.getUrlWithPriority(2);
                        count=MAX_PAGES - listUrl.size();

                        for (int i = 0; i < count; i++)
                        {
                            listUrl.add(listUrl3.get(i));
                        }


                    }


                }
            }
            else if (priority == 1)
            {
                // priority 0,1 , unvisited , 2
                List<String[]> listUrl2 = db.getUrlWithPriority(1);
                count=MAX_PAGES - listUrl.size();
                if (listUrl2.size() >= count)
                {
                    for (int i = 0; i < count; i++)
                    {
                        listUrl.add(listUrl2.get(i));
                    }

                }
                else {

                    for (int i = 0; i < listUrl2.size(); i++)
                    {
                        listUrl.add(listUrl2.get(i));
                    }
                    count=MAX_PAGES - listUrl.size();
                    if (unvisitedCount >= count)
                    {
                        for (int i = 0; i < count; i++)
                        {
                            String[] url = db.getUnvisitedUrl();
                            listUrl.add(url);
                        }
                    }
                    else {
                        for (int i = 0; i < unvisitedCount; i++)
                        {
                            String[] url = db.getUnvisitedUrl();
                            listUrl.add(url);
                        }

                        List<String[]> listUrl3 = db.getUrlWithPriority(2);
                        count=MAX_PAGES - listUrl.size();
                        for (int i = 0; i < count; i++)
                        {
                            listUrl.add(listUrl3.get(i));
                        }
                    }
                }


            }
            else {
                //priority =2;
                // priority 0,1,2 unvisited
                List<String[]> listUrl2 = db.getUrlWithPriority(1);

                if (listUrl2.size() >= (MAX_PAGES - listUrl.size()))
                {

                    for (int i = 0; i < (MAX_PAGES - listUrl.size()); i++)
                    {
                        listUrl.add(listUrl2.get(i));
                    }
                }
                else {
                    for (int i = 0; i < listUrl2.size(); i++)
                    {
                        listUrl.add(listUrl2.get(i));
                    }
                    List<String[]> listUrl3 = db.getUrlWithPriority(2);
                    count=MAX_PAGES - listUrl.size();

                    for (int i = 0; i <count; i++)
                    {
                        listUrl.add(listUrl3.get(i));
                    }

                }


            }
            db.saveURLSetForCrawling(listUrl);
            db.setPriorityState(priority+1);
            try {
                FileWriter fileWriter =new FileWriter("input.txt",true);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                bufferedWriter.newLine();
                bufferedWriter.write("recrawling");
                bufferedWriter.close();



            }
            catch(IOException ex) {
                System.out.println( "Error writing to file '");

            }

        }



        int target=db.getStoppingCreatria();
        int stopCreatria=(MAX_PAGES-target)/numOfThreads;
        int stopCreatria2=((MAX_PAGES-target)/numOfThreads)+((MAX_PAGES-target)%numOfThreads);

        threadArray = new Crawler[numOfThreads]; //dynamic array of crawler
        Thread[] temp= new Thread [numOfThreads] ;

        for(int i=0 ; i<numOfThreads;i++)
        {

            if(i==0)
            {
                threadArray[i]=new Crawler(stopCreatria2,db,1);
            }
            else
            {
                threadArray[i]=new Crawler(stopCreatria,db,1);
            }

            temp[i]= new Thread(threadArray[i]);
            temp[i].start();


        }

        for(int i=0 ; i<numOfThreads;i++)
        {
            temp[i].join();
        }
        db.clearStoppingCritria();
        firstTime=1;

        db.incCurrentCrawelVersion();

    }







    void calculatePageRank ()
    {
        List<Bundle> dataCollection1 = new ArrayList<>();
        dataCollection1 = db.getCollection1();
        HashMap<String,Integer> urlVsId = new HashMap<>();
        for (int i = 0; i < dataCollection1.size(); i++)
        {
            urlVsId.put(dataCollection1.get(i).getUrl(), new Integer(i));
            //hash map of the urls vs it's location in datacollection1
            //to be able to search by urls
            //check if DB htrg3homli bnfs el trtib fi 100 mara yb2a el hash da mara w7da bas
        }

        for (int k = 0; k < 100; k++)
        {


            for (int i = 0; i < dataCollection1.size(); i++)
            {
                //TODO @Brenda
                // why division by dataCollection1.size() ??


                double pageRank = (0.15)/dataCollection1.size();
                List<String> parents = new ArrayList<>();
                parents = dataCollection1.get(i).getParents();

                for (int j = 0; j < parents.size(); j++)
                {
                    String parentUrl = parents.get(j);
                    int getHashID=-1;
                    try {
                         getHashID=urlVsId.get(parentUrl);
                    }
                    catch(Exception e)
                    {
                        if (parentUrl.charAt(parentUrl.length()-1) == '/')
                            parentUrl = parentUrl.substring(0,parentUrl.length() - 1);
                        else parentUrl = parentUrl + '/';
                        getHashID=urlVsId.get(parentUrl);
                    }
                    double parentRank=dataCollection1.get(getHashID).getPageRank();
                    int childOfParentNum=dataCollection1.get(getHashID).getChilds().size();
                    if(childOfParentNum==0)
                        childOfParentNum=1;//could happen if it's a seed fa m7dsh gabo da ehna gybino kda
                    pageRank += (0.85) * (parentRank/childOfParentNum );
                    if(pageRank==0.85003)
                    {
                        int test=0;
                    }
                }

                db.setPageRank(dataCollection1.get(i).getUrl(),pageRank);
                dataCollection1.get(i).setPageRank(pageRank);

                System.out.println("**PAGERANK ,inter no: "+i);
            }


        }
        int x=0;
                /*
        1- make saveBundle and edit in recrawling to take page rank or set it to one
        ,save pagerank in DB
        2- nzabat eli f modelDB enha t save rank m3 b2i hagat
        3- calculate pageRank abl kol reCrawel sah ?
        */
    }



}