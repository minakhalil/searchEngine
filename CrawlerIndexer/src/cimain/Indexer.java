package cimain;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;


import java.util.List;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


public class Indexer {

    public static final int THREAD_COUNT = 10;
    public static java.util.Date startTime = new java.util.Date();
    private static MongoDatabase mongoDB = new MongoDatabase();
    private static Model_DB ModelDB = new Model_DB();
    private static DB dataBase = mongoDB.connectMongo();
    private static DBCollection indexerCollection = dataBase.getCollection("collection1");


    public static void main(String[] args) {


        final DB connectionArr[] = new DB[THREAD_COUNT];


        for (int j = 0; j < THREAD_COUNT; j++) {
            MongoDatabase mong = new MongoDatabase();
            connectionArr[j] = mong.connectMongo();
        }

        Timer t = new Timer();
        t.schedule(new TimerTask() {

            @Override
            public void run() {

                Thread.currentThread().setName("THR0");
                Object lock = new Object();

                DBObject matchedFromDB = new BasicDBObject();
                matchedFromDB.put("indexed", false);
                List<DBObject> documents = indexerCollection.find(matchedFromDB).toArray();

                if (documents.size() > THREAD_COUNT) {

                    Thread threads[] = new Thread[THREAD_COUNT];
                    int step = documents.size() / THREAD_COUNT;

                    for (int j = 0; j < THREAD_COUNT; j++) {
                        int startPosition = j * step;
                        int endPosition = (j == (THREAD_COUNT - 1)) ? (documents.size()) : startPosition + step;
                        List inputListForThread = documents.subList(startPosition, endPosition);
                        threads[j] = new Thread(new IndexerFilter(inputListForThread, lock, connectionArr[j], ModelDB), "THR" + (j + 1));
                        threads[j].start();

                    }


                    for (int j = 0; j < THREAD_COUNT; j++) {

                        try {

                            threads[j].join();

                        } catch (Exception p) {

                        }

                    }


                } else {

                    IndexerFilter iFilter = new IndexerFilter(documents, lock, connectionArr[0], ModelDB);
                    iFilter.run();
                }

                if (documents.size() > 0) {

                    DBObject query = new BasicDBObject();
                    query.put("$set", new BasicDBObject().append("indexed", true));

                    indexerCollection.updateMulti(matchedFromDB, query);
                }

                java.util.Date endTime = new java.util.Date();

                System.out.println("---FINISH-going to sleep (" + startTime + " -> " + endTime + ")\n");

            }
        }, 0, 5000);
//
    }


}
