package cimain;

import com.mongodb.*;


import com.mongodb.async.SingleResultCallback;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.client.model.*;
import org.bson.Document;
import org.bson.types.ObjectId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class IndexerController {



    private DB dataBase;
    private Utilities util = new Utilities();
    private DBCollection indexerCollection;


    private String currentURL;
    private int crawlerVersion;



    private List<ObjectId> toBeRemoveUrlIDs ;


    private List<KeywordModel> keywordDetailList;

    private int currentInsertedCount,currentUpdatedCount;

    IndexerController(List<KeywordModel> keys,int crawlerVersion,String currentURL,DB mDBO,MongoCollection c,Object l){


        this.currentURL = currentURL;
        this.crawlerVersion = crawlerVersion;
        this.keywordDetailList = new ArrayList<>(keys);
        this.dataBase = mDBO;

        toBeRemoveUrlIDs = new ArrayList<ObjectId>();


        indexerCollection = dataBase.getCollection("collection2");




    }



    public void usertCurrentWords(){

        BulkWriteOperation builder = indexerCollection.initializeUnorderedBulkOperation();

        for(int i=0;i<keywordDetailList.size();i++) {



            DBObject toBeUpdateObject =new BasicDBObject();
            toBeUpdateObject.put("keyword", keywordDetailList.get(i).getKeyword());




            Document bDido = new Document("crawel_version", crawlerVersion);



            bDido.append("repetitions", keywordDetailList.get(i).getRepetitions());



            List<Document> dd = new ArrayList<>();


            for(int j=0;j<keywordDetailList.get(i).getRepetitions();j++) {

                Document eRD = new Document();
                eRD.put("phrase", keywordDetailList.get(i).getPhrase().get(j).toString());
                eRD.put("location", keywordDetailList.get(i).getLocations().get(j).toString());
                eRD.put("position", keywordDetailList.get(i).getPositions().get(j).toString());



                dd.add(eRD);

            }




            bDido.put("data", dd);

            BasicDBObject updateQueryAppendToArray =new BasicDBObject("$addToSet", new BasicDBObject("urls", currentURL));

            BasicDBObject setOnInsertObject = new BasicDBObject();
            setOnInsertObject.append("keyword", keywordDetailList.get(i).getKeyword());

            updateQueryAppendToArray.append("$setOnInsert", setOnInsertObject);
            updateQueryAppendToArray.append("$set", new BasicDBObject("details."+util.removeDotsFromUrl(currentURL), bDido));


            builder.find(toBeUpdateObject).upsert().updateOne(updateQueryAppendToArray);


        }


        if(keywordDetailList.size()>0) {
            try {
                BulkWriteResult result = builder.execute();
                currentInsertedCount = result.getInsertedCount();
                currentUpdatedCount = result.getModifiedCount();
            }catch (MongoSocketReadTimeoutException e){

            }
        }
    }





    public void removeUrlFromOldWords(){

        DBObject oldCrawledWordsForThisUrl =new BasicDBObject();
        oldCrawledWordsForThisUrl.put("urls", currentURL);

        List<DBObject> toBeFilteredDocuments = indexerCollection.find(oldCrawledWordsForThisUrl).toArray();

        for (int i=0;i<toBeFilteredDocuments.size();i++) {

            DBObject ob = (DBObject)(toBeFilteredDocuments.get(i)).get("details");
            DBObject urlDetails = (DBObject)ob.get(util.removeDotsFromUrl(currentURL));
            String id = toBeFilteredDocuments.get(i).get("_id").toString();
            int crawelVersion = Integer.parseInt(urlDetails.get("crawel_version").toString());

            if(crawelVersion < crawlerVersion){
                toBeRemoveUrlIDs.add(new ObjectId(id));
            }


        }


        DBObject toBeUpdateRemovedUrlObjects =new BasicDBObject();
        toBeUpdateRemovedUrlObjects.put("_id", new BasicDBObject("$in", toBeRemoveUrlIDs));


        DBObject updateQueryRemoveUrl = new BasicDBObject();
        updateQueryRemoveUrl.put("$pull", new BasicDBObject().append("urls", currentURL));
        updateQueryRemoveUrl.put("$unset", new BasicDBObject().append("details."+util.removeDotsFromUrl(currentURL), 1));

        indexerCollection.updateMulti(toBeUpdateRemovedUrlObjects,updateQueryRemoveUrl);


    }

    public void logStatistics(){

        System.out.println("INDEXER("+Thread.currentThread().getName()+"): finish url(" + currentURL + ")");
        System.out.print("  *insertedOrUpdated("+currentUpdatedCount+")");
        System.out.print("  *removedWords("+toBeRemoveUrlIDs.size()+")");
        System.out.println("");
    }






    public void finishIndexing(){

        usertCurrentWords();
        removeUrlFromOldWords();
        logStatistics();
        keywordDetailList.clear();

    }

}