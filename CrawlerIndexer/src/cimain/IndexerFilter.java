package cimain;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import com.mongodb.async.client.MongoCollection;
import org.bson.types.ObjectId;
import org.tartarus.snowball.ext.englishStemmer;


public class IndexerFilter implements Runnable {



    private  DB dataBase;
    private Model_DB modelDB;
    private  MongoAsyncDatabase mongoAsyncDB ;
    private MongoCollection col2 ;


    private String currentURL;
    private int crawlerVersion ;
    private List<DBObject> documents;
    private Object lock;
    private List<KeywordModel> keywordDetailList = new ArrayList<KeywordModel>();


    private  String meta;
    private  String header;
    private  String body;

    private List<String> stopWords;


    public IndexerFilter(List<DBObject> d, Object l,DB u,Model_DB mdb){
        stopWords=new ArrayList<String>();

        documents = new ArrayList<DBObject>(d);
        readStopWords();
        crawlerVersion = mdb.getCurrentCrawlerVersion();
        lock=l;
        dataBase=u;


    }

    public String getCurrentURL() {
        return currentURL;
    }

    public int getCrawlerVersion() {
        return crawlerVersion;
    }

    public List<KeywordModel> getKeywordDetailList() {
        return keywordDetailList;
    }

    private void readStopWords() {
        String line;
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader("stop_words.txt"));

            while((line = bufferedReader.readLine()) != null) {
                stopWords.add(line);
            }

            bufferedReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch(IOException ex) {
            ex.printStackTrace();
        }

    }

    private boolean isStopWord(String s){
        return stopWords.contains(s);
    }

    private String getphrase(List<String> l,int index){

        String ii = new String();

        if(l.size()<=5){
            StringBuilder sb = new StringBuilder();
            for (String s : l)
            {
                sb.append(s);
                sb.append(" ");
            }

            ii = sb.toString();
            return ii;

        }else if(index<=1){
            StringBuilder sb = new StringBuilder();
            for (String s : l.subList(0,5))
            {
                sb.append(s);
                sb.append(" ");
            }

            ii = sb.toString();
            return ii;
        }else if(index>=l.size()-2){
            StringBuilder sb = new StringBuilder();
            for (String s : l.subList(l.size()-5,l.size()))
            {
                sb.append(s);
                sb.append(" ");
            }

            ii = sb.toString();
            return ii;
        }


        StringBuilder sb = new StringBuilder();
        for (String s : l.subList(index-2,index+3))
        {
            sb.append(s);
            sb.append(" ");
        }

        ii = sb.toString();
        return ii;
    }

    public void run(){





        for (int i=0;i<documents.size();i++) {

            List<String> metaList=new ArrayList<String>();
            List<String> headerList=new ArrayList<String>();
            List<String> bodyList=new ArrayList<String>();


            currentURL = documents.get(i).get("url").toString();

            meta = ((((DBObject)documents.get(i).get("meta")).get("description")) == null) ? "" :((DBObject)documents.get(i).get("meta")).get("description").toString() ;
            header = documents.get(i).get("header").toString();
            body = documents.get(i).get("body").toString();


            //author bishoy kamal

            meta=meta.replaceAll(" @\\s+ ","");
            header=header.replaceAll(" @\\s+ ","");
            body=body.replaceAll(" @\\s+ ","");

            meta=meta.replaceAll("[^a-zA-Z0-9' ]"," ");
            header=header.replaceAll("[^a-zA-Z0-9' ]"," ");
            body=body.replaceAll("[^a-zA-Z0-9' ]"," ");

            if(!meta.equals(""))
                metaList= Arrays.asList(meta.split("\\s+"));

            if(!header.equals(""))
                headerList= Arrays.asList(header.split("\\s+"));

            if(!body.equals(""))
                bodyList= Arrays.asList(body.split("\\s+"));

            englishStemmer stemmer=new englishStemmer();
            Set<String> words=new TreeSet<String>();

//                System.out.println("Meta: "+metaList + " i "+metaList.size());
//                System.out.println("Header: "+headerList + " i "+headerList.size());
//                System.out.println("Body: "+bodyList + " i "+bodyList.size());
            for(int k=0;k<bodyList.size();k++){

                String t=bodyList.get(k);
                String lower=t.toLowerCase();
                stemmer.setCurrent(lower);
                stemmer.stem();
                String ii = stemmer.getCurrent();
                if(!isStopWord(lower)){

                    KeywordModel ktemp = new KeywordModel();
                    ktemp.setKeyword(ii);

                    //System.out.println("current: "+ii);
                    if(words.contains(ii)){
                        int keywordIndexinList = keywordDetailList.indexOf(ktemp);
                        ktemp=keywordDetailList.get(keywordIndexinList);
                        ktemp.setRepetitions(ktemp.getRepetitions()+1);
                        ktemp.addPhrase(getphrase(bodyList,k));
                        ktemp.addLocations("body");
                        ktemp.addOriginalWord(t);
                        ktemp.addPosition(k);


                    }else {
                        //ktemp.initialize();

                        ktemp.addPhrase(getphrase(bodyList,k));
                        ktemp.addLocations("body");
                        ktemp.addOriginalWord(t);
                        ktemp.addPosition(k);
                        keywordDetailList.add(ktemp);
                        words.add(ii);

                    }
                }
            }



            for(int k=0;k<metaList.size();k++){

                String t=metaList.get(k);
                String lower=t.toLowerCase();

                stemmer.setCurrent(lower);
                stemmer.stem();
                String ii = stemmer.getCurrent();
                if(!isStopWord(lower)){

                    KeywordModel ktemp = new KeywordModel();
                    ktemp.setKeyword(ii);
                    if(words.contains(ii)){
                        int keywordIndexinList = keywordDetailList.indexOf(ktemp);
                        ktemp=keywordDetailList.get(keywordIndexinList);
                        ktemp.setRepetitions(ktemp.getRepetitions()+1);
                        ktemp.addPhrase(getphrase(metaList,k));
                        ktemp.addLocations("meta");
                        ktemp.addOriginalWord(t);
                        ktemp.addPosition(k);



                    }else {
                        //ktemp.initialize();

                        ktemp.addPhrase(getphrase(metaList,k));
                        ktemp.addLocations("meta");
                        ktemp.addOriginalWord(t);
                        ktemp.addPosition(k);
                        keywordDetailList.add(ktemp);
                        words.add(ii);


                    }
                }
            }



            for(int k=0;k<headerList.size();k++){
                String t=headerList.get(k);

                String lower=t.toLowerCase();

                stemmer.setCurrent(lower);
                stemmer.stem();
                String ii = stemmer.getCurrent();

                if(!isStopWord(lower)){

                    KeywordModel ktemp = new KeywordModel();
                    ktemp.setKeyword(ii);
                    if(words.contains(ii)){
                        int keywordIndexinList = keywordDetailList.indexOf(ktemp);
                        ktemp=keywordDetailList.get(keywordIndexinList);
                        ktemp.setRepetitions(ktemp.getRepetitions()+1);
                        ktemp.addPhrase(getphrase(headerList,k));
                        ktemp.addLocations("header");
                        ktemp.addOriginalWord(t);
                        ktemp.addPosition(k);



                    }else {
                        // ktemp.initialize();


                        ktemp.addPhrase(getphrase(headerList,k));
                        ktemp.addLocations("header");
                        ktemp.addOriginalWord(t);
                        ktemp.addPosition(k);
                        keywordDetailList.add(ktemp);
                        words.add(ii);


                    }
                }
            }


            IndexerController iController = new IndexerController(getKeywordDetailList(), getCrawlerVersion(), getCurrentURL(),dataBase,col2,lock);
            iController.finishIndexing();
            if((documents.size()-1)>0)
                System.out.println("          " +(i*100/(documents.size()-1))+"%" );
            keywordDetailList.clear();
            words.clear();


        }




    }



}
