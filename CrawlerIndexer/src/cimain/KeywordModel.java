package cimain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class KeywordModel {

    public KeywordModel () {

        phrase=new ArrayList<String>();
        locations=new ArrayList<String>();
        positions=new ArrayList<Integer>();
        originalWord=new ArrayList<String>();

    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getRepetitions() {
        return repetitions;
    }

    public void setRepetitions(int repetitions) {
        this.repetitions = repetitions;
    }

    public List<String> getPhrase() {
        return phrase;
    }

    public void setPhrase(List<String> phrase) {
        this.phrase = phrase;
    }

    public List<String> getLocations() {
        return locations;
    }

    public void setLocations(List<String> locations) {
        this.locations = locations;
    }

    public List<Integer> getPositions() {
        return positions;
    }

    public void setPositions(List<Integer> positions) {
        this.positions = positions;
    }
    //bishoy: to add new element to Lists
    public void addPhrase(String s){
        this.phrase.add(s);
    }
    public void addPosition(Integer s){
        this.positions.add(s);
    }
    public void addLocations(String s){
        this.locations.add(s);
    }
    public void addOriginalWord(String s){
        this.originalWord.add(s);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KeywordModel that = (KeywordModel) o;
        return Objects.equals(keyword, that.keyword);
    }

    public List<String> getOriginalWord() {
        return originalWord;
    }

    public void setOriginalWord(List<String> originalWord) {
        this.originalWord = originalWord;
    }

    @Override
    public int hashCode() {

        return Objects.hash(keyword);
    }

    private String keyword;
    private int repetitions = 1;
    private List<String> phrase;
    private List<String> locations;//meta or body or header
    private List<Integer> positions;//bishoy: convert to List to use add
    private List<String> originalWord;//bishoy: word before stemmin

}
