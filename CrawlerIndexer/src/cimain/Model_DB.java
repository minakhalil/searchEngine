package cimain;
import com.mongodb.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Brenda
 */

public class Model_DB {

    private DB dataBase;

    public Model_DB(){

        MongoDatabase mongoDB = new MongoDatabase();
        dataBase = mongoDB.connectMongo();
    }
    public  DBCollection initDBConnection (String collection)
    {

        DBCollection col = dataBase.getCollection(collection);
        return col;
    }

    public synchronized boolean saveBundle (Bundle B)
    {
        /*
         * @MKA
         * Function Saves Bundle object in crawler collection
         *
         *  NOT TESTED
         */

        DBCollection col = initDBConnection("collection1");

        BasicDBObject ob = new BasicDBObject();
        ob.put("url",B.getUrl());

        ob.put("childs",(new MongoDatabase()).arrayToDBObject(B.getChilds()));
        ob.put("parents",(new MongoDatabase()).arrayToDBObject(B.getParents()));

        BasicDBObject metaOB = new BasicDBObject();
        metaOB.put("robots",B.getRobots());
        metaOB.put("descritption",B.getDescription());

        ob.put("meta",metaOB);
        ob.put("title",B.getTitle());
        ob.put("header",B.getHeader());
        ob.put("body",B.getBody());
        ob.put("indexed",B.getIndexed());
        ob.put("priority",B.getPriority());
        ob.put("pageRank",B.getPageRank());
        ob.put("hash",B.getHash());

        try {
            col.insert(ob);
            return true;
        }catch (Exception e){
            return false;
        }


    }


    public synchronized void saveUnvisitedUrl(List<String[]> child)
    {


        DBCollection col = initDBConnection("collection3");
        BulkWriteOperation builder = col.initializeUnorderedBulkOperation();


        for(int i=0;i<child.size();i++){

            BasicDBList innerList = new BasicDBList();

            innerList.add(child.get(i)[0]);
            innerList.add(child.get(i)[1]);

            BasicDBObject oo = new BasicDBObject("data", innerList);
            BasicDBObject oe = new BasicDBObject("$set",new BasicDBObject("data", innerList));
            builder.find(oo).upsert().updateOne(oe);

        }

        try {

            if(child.size()>0)
                builder.execute();

        }catch (Exception e){

        }


    }
    public synchronized String [] getUnvisitedUrl()
    {
        /*
         * @MKA
         * Function gets first element in unvisitedURLs schema then remove it
         *
         * NOT TESTED
         **/
        String [] url = new String [2];

        DBCollection col = initDBConnection("collection3");
        BasicDBObject query = new BasicDBObject();
        DBObject ob = col.findOne();
        if(ob==null) return null;


        BasicDBList objects = (BasicDBList)((DBObject)ob.get("data"));
        url[0] = objects.toArray()[0].toString();
        url[1] = objects.toArray()[1].toString();
        col.remove(ob);

        return url;
    }

    public synchronized void incrementCounterOfStoppingCreatria()
    {
        // to do fi counter fi database hanzwedo lma nkhalas haga
        DBCollection col = initDBConnection("collection4");

        BasicDBObject query = new BasicDBObject();

        BasicDBObject updateQ = new BasicDBObject("$inc",new BasicDBObject("stoppingCritria",1));

        col.update(query,updateQ);



    }

    public  int  getStoppingCreatria() {

        DBCollection col = initDBConnection("collection4");

        DBObject ob = col.findOne();
        return Integer.parseInt(ob.get("stoppingCritria").toString());

    }



    public synchronized void getVisitedUrl(Set<String> pagesVisited) {

        DBCollection col = initDBConnection("collection1");

        BasicDBObject query= new BasicDBObject("","").append("",new BasicDBObject("url","1"));

        List<DBObject> urls = col.find().toArray();

        for(int i=0;i<urls.size();i++){

            String url = urls.get(i).get("url").toString();
            pagesVisited.add(url);
        }



    }



    public void saveURLSetForCrawling(List<String[]> list)
    {
        //save them as current crawling


        DBCollection col = initDBConnection("collection5");

        BulkWriteOperation builder = col.initializeUnorderedBulkOperation();


        for(int i=0;i<list.size();i++){

            BasicDBList innerList = new BasicDBList();

            innerList.add(list.get(i)[0]);
            innerList.add(list.get(i)[1]);

            BasicDBObject oo = new BasicDBObject("data", innerList);
            BasicDBObject oe = new BasicDBObject("$set",new BasicDBObject("data", innerList));
            builder.find(oo).upsert().updateOne(oe);
        }

        try {

            if(list.size()>0)
                builder.execute();

        }catch (Exception e){

        }
    }


    public int getPriorityState()
    {
        //get priority state
        int result=0;

        DBCollection col = initDBConnection("collection4");

        DBObject ob = col.findOne();
        result = Integer.parseInt(ob.get("priorityState").toString());





        //setPriorityState(result+1);
        return result;
    }
    public void  setPriorityState(int num)
    {
        int x = getPriorityState();
        if(x==2)
            num=0;

        DBCollection col = initDBConnection("collection4");

        BasicDBObject query = new BasicDBObject();

        BasicDBObject updateQ = new BasicDBObject("$set",new BasicDBObject("priorityState",num));

        col.update(query,updateQ);

    }

    public synchronized String[] getURLReCrawling ()
    {
        //return w7da b3d ma yshelha men table


        String [] url = new String [2];

        DBCollection col = initDBConnection("collection5");
        BasicDBObject query = new BasicDBObject();
        DBObject ob = col.findAndRemove(query);

        BasicDBList objects = (BasicDBList)((DBObject)ob.get("data"));
        url[0] = objects.toArray()[0].toString();
        url[1] = objects.toArray()[1].toString();



        return url;

    }

    public int getUnvisitedCount()
    {
        DBCollection col = initDBConnection("collection3");
        long size = col.getCount();
        return (int)size;
    }

    public void saveNewParent(String urlParent,String urlChild)
    {
        DBCollection col = initDBConnection("collection1");
        BasicDBObject query = new BasicDBObject("url",urlChild);

        DBObject updateQueryAppendToArray = new BasicDBObject();
        updateQueryAppendToArray.put("$addToSet", new BasicDBObject().append("parents", urlChild));

        col.update(query,updateQueryAppendToArray);


    }

    public void clearStoppingCritria(){

        DBCollection col = initDBConnection("collection4");

        BasicDBObject query = new BasicDBObject();

        BasicDBObject updateQ = new BasicDBObject("$set",new BasicDBObject("stoppingCritria",0));

        col.update(query,updateQ);

    }

    public boolean isVisited(String url){


        DBCollection col = initDBConnection("collection1");
        BasicDBObject query = new BasicDBObject("url",url);
        List<DBObject> urls = col.find(query).toArray();
        if(urls.size()>0) return true;

        if (url.charAt(url.length()-1) == '/') url = url.substring(0,url.length() - 1);
        else url = url + '/';

        query = new BasicDBObject("url",url);
        urls = col.find(query).toArray();
        if(urls.size()>0) return true;

        return false;
    }

    public Bundle getBundle(String url){

        DBCollection col = initDBConnection("collection1");
        BasicDBObject query = new BasicDBObject("url",url);
        DBObject oo = col.findOne(query);
        if(oo==null){
            if (url.charAt(url.length()-1) == '/') url = url.substring(0,url.length() - 1);
            else url = url + '/';
            query = new BasicDBObject("url",url);
            oo = col.findOne(query);
        }
        Bundle b = new Bundle();
        MongoDatabase mdb = new MongoDatabase();

        if(oo==null)
            System.out.println("el 5raaa aho: "+url);

        String strDummy = (oo.get("body") == null)?"":oo.get("body").toString();
        b.setBody(strDummy);

        DBObject meta = (DBObject) oo.get("meta");
        b.setRobots(meta.get("robots").toString());
        b.setDescription(meta.get("descritption").toString());
        b.setPriority(Integer.parseInt(oo.get("priority").toString()));
        b.setPageRank(Double.parseDouble(oo.get("pageRank").toString()));
        b.setHeader(oo.get("header").toString());
        b.setTitle(oo.get("title").toString());
        b.setIndexed((Boolean) oo.get("indexed"));
        b.setChild(mdb.getArrayFromMongoDocument(oo,"childs"));
        b.setParent(mdb.getArrayFromMongoDocument(oo,"parents"));

        b.setHash(oo.get("hash").toString());



        return b;

    }

    public List<String[]> getUrlWithPriority(int p)
    {

        List<String[]> toBeReturned = new ArrayList<String[]>();


        DBCollection col = initDBConnection("collection1");
        BasicDBObject query = new BasicDBObject("priority",p);
        List<DBObject> urls = col.find(query).toArray();

        for(int i=0;i<urls.size();i++){
            String[] l = new String[2];
            l[0]=urls.get(i).get("url").toString();
            l[1]=((new MongoDatabase()).getArrayFromMongoDocument(urls.get(i),"parents")).get(0).toString();
            toBeReturned.add(l);
        }



        return toBeReturned;

    }

    public void saveBundle2(Bundle b)
    {


        DBCollection col = initDBConnection("collection1");
        BasicDBObject query = new BasicDBObject("url",b.getUrl());

        BasicDBObject ob = new BasicDBObject();
        ob.put("url",b.getUrl());

        ob.put("childs",(new MongoDatabase()).arrayToDBObject(b.getChilds()));
        ob.put("parents",(new MongoDatabase()).arrayToDBObject(b.getParents()));

        BasicDBObject metaOB = new BasicDBObject();
        metaOB.put("robots",b.getRobots());
        metaOB.put("descritption",b.getDescription());

        ob.put("meta",metaOB);
        ob.put("title",b.getTitle());
        ob.put("header",b.getHeader());
        ob.put("body",b.getBody());
        ob.put("indexed",b.getIndexed());
        ob.put("priority",b.getPriority());
        ob.put("pageRank",b.getPageRank());
        ob.put("hash",b.getHash());

        col.update(query,ob);


    }


    public void increasePriority(String url){

        //-1
        DBCollection col = initDBConnection("collection1");

        BasicDBObject query = new BasicDBObject("url",url);


        DBObject toBeUpdated = new BasicDBObject();
        DBObject oo = col.findOne(query);
        if(oo==null){
            if (url.charAt(url.length()-1) == '/') url = url.substring(0,url.length() - 1);
            else url = url + '/';
            query = new BasicDBObject("url",url);
            oo = col.findOne(query);
        }
        int p = Integer.parseInt(oo.get("priority").toString());
        if(p<=1){
            toBeUpdated.put("$set", new BasicDBObject().append("priority", p+1));
            col.update(query,toBeUpdated);
        }
    }


    public void decreasePriority(String url){

        //+1
        DBCollection col = initDBConnection("collection1");

        BasicDBObject query = new BasicDBObject("url",url);


        DBObject toBeUpdated = new BasicDBObject();
        DBObject oo = col.findOne(query);
        if(oo==null){
            if (url.charAt(url.length()-1) == '/') url = url.substring(0,url.length() - 1);
            else url = url + '/';
            query = new BasicDBObject("url",url);
            oo = col.findOne(query);
        }
        int p = Integer.parseInt(oo.get("priority").toString());
        if(p>0){
            toBeUpdated.put("$set", new BasicDBObject().append("priority", p-1));
            col.update(query,toBeUpdated);
        }
    }

    public int getUrlReCrawledCount()
    {
        DBCollection col = initDBConnection("collection5");
        long size = col.getCount();
        return (int)size;

    }

    public List<String[]> getReCrawlingList()
    {
        List<String[]>result= new ArrayList<>();
        return result;
    }

    public void incCurrentCrawelVersion(){


        DBCollection col = initDBConnection("collection4");

        BasicDBObject query = new BasicDBObject();

        BasicDBObject updateQ = new BasicDBObject("$inc",new BasicDBObject("crawelVersion",1));

        col.update(query,updateQ);

    }

    public  int  getCurrentCrawlerVersion() {

        DBCollection col = initDBConnection("collection4");

        DBObject ob = col.findOne();
        return Integer.parseInt(ob.get("crawelVersion").toString());

    }

    public  synchronized void insertOrUpdateInSubscribtion (String url)
    {
        //check if this url is in collection 6 (collection of link and version)
        //if it does , updated the version with the current crawel version by
        //calling get Current Crawler version
        // else insert with in the table
        DBCollection col = initDBConnection("subscriptions");

        DBObject toBeUpdateObject =new BasicDBObject();
        toBeUpdateObject.put("url", url);

        BasicDBObject updateQuery =new BasicDBObject("$set", new BasicDBObject("version",getCurrentCrawlerVersion()));

        col.update(toBeUpdateObject,updateQuery,true,false);



    }/*
1- make saveBundle and edit in recrawling to take page rank or set it to one
,save pagerank in DB
*/
    public List<Bundle> getCollection1()
    {
        //to do get table 1 AHHH WALAHI MTSHTMNISSH , BS BDEL MA A3MELK
        //900 ALF HIT 3ALA DATABASE AW23hlk i need to make 100 iteration 3lehom
        // w kol iteration b7tag hagat men urls tania
        List<Bundle> listUrl= new ArrayList<>();

        DBCollection col = initDBConnection("collection1");

        DBObject matchedFromDB = new BasicDBObject();
        col.find(matchedFromDB);

        List<DBObject> documents = col.find(matchedFromDB).toArray();
        for(int i=0;i<documents.size();i++){

            Bundle b = new Bundle();
            DBObject oo = documents.get(i);
            MongoDatabase mdb = new MongoDatabase();

            b.setPageRank(Double.parseDouble(oo.get("pageRank").toString()));
            b.setChild(mdb.getArrayFromMongoDocument(oo,"childs"));
            b.setParent(mdb.getArrayFromMongoDocument(oo,"parents"));
            b.setUrl(oo.get("url").toString());
            listUrl.add(b);


        }
        return listUrl;
    }

    public void setPageRank(String url,double rank){

        DBCollection col = initDBConnection("collection1");

        BasicDBObject query = new BasicDBObject("url",url);



        DBObject oo = col.findOne(query);
        if(oo==null){
            if (url.charAt(url.length()-1) == '/') url = url.substring(0,url.length() - 1);
            else url = url + '/';
            query = new BasicDBObject("url",url);

        }

        BasicDBObject oe = new BasicDBObject("$set",new BasicDBObject("pageRank", rank));
        col.update(query,oe);
    }




}