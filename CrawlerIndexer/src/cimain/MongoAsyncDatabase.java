package cimain;

import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;
import org.bson.Document;

public class MongoAsyncDatabase {

    public MongoCollection<Document> connectAsyncMongoAndGetCollection(){

        MongoClient mongoClient = MongoClients.create("mongodb://"+"localhost");
        MongoDatabase database =  mongoClient.getDatabase("search-engine");
        return database.getCollection("collection2");
    }


}