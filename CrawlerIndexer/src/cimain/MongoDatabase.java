package cimain;
import com.mongodb.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MongoDatabase {

    public String ip = "localhost";
    public int port = 27017;
    public String db = "search-engine";

    public  DB connectMongo(){


        MongoClientOptions.Builder builder = new MongoClientOptions.Builder();
        builder.maxConnectionIdleTime(6000000);
        builder.socketTimeout(6000000);
        builder.socketKeepAlive(true);
        builder.connectTimeout(1000000000);
        builder.serverSelectionTimeout(1000000000);

        builder.socketKeepAlive(true);
        MongoClientOptions opts = builder.build();



        MongoClient mongo = new MongoClient(new ServerAddress(ip,port),opts);

        Logger monologger=Logger.getLogger("org.mongodb.driver");
        monologger.setLevel(Level.SEVERE);
        DB dataBase = mongo.getDB(db);
        return dataBase;
    }



    public  List<String> getArrayFromMongoDocument(DBObject document,String key){
        List<String> m = new ArrayList<String>();
        BasicDBList objects = (BasicDBList) ((DBObject) document.get(key));
        for(int i=0;i<objects.size();i++) {
            m.add(objects.get(i).toString());
        }
        return m;
    }

    public  BasicDBList arrayToDBObject(List<String> array){

        BasicDBList basicDBList = new BasicDBList();

        for(int i=0;i<array.size();i++){
            basicDBList.add(array.get(i));
        }
        return basicDBList;
    }



}
