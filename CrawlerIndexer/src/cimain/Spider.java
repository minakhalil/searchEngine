package cimain;



import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import javax.xml.bind.DatatypeConverter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Bassem
 * added the jsoup-1.11.2.jar
 * CTRL + SHIFT + ALT + S -> modules -> dependencies -> + jar
 */
public class Spider{

    private final String USER_AGENT =
            //"Chrome/13.0.782.112";
            "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0 ";
    /**
     * the url that this spider object is supposed to visit and handle it's html files
     */
    private String url;

    /**
     * the HTML document file downloaded from the seed link
     */
    private Document htmlDocument;

    /**
     * list of links found on page
     */
    private Elements links;
    private List<String> httpLinks = new ArrayList<>();


    StringBuilder ROBOTS;

    /**
     * the bundle that will be filled and returned
     */
    private Bundle data;

    /**
     * checked by the class who owns the spider as a state flag
     */
    public boolean success;

    /**
     * number of links that can be returned by this spider
     */
    public int linksCount;

    {
        htmlDocument = null;
        success = false;
        linksCount = 0;
        data = new Bundle();
        ROBOTS = new StringBuilder();
    }


    Spider(String url)
    {
        try {
            String newUrl = normalize(url);
            System.out.println("new Spider initialized, visiting " + newUrl);
            this.url = newUrl;
            crawl();

        } catch (MalformedURLException e) {
            System.out.println("not a valid URL, can't initialize spider with " + url);
            this.success = false;
            //e.printStackTrace();
        }
    }

    private void crawl ()
    {
        //if (true)
        //while (true) {}

        final Document document = downloadDocument();
        if (document != null && checkRobots()) {
            findLinks();
            fillBundleData();
            System.out.println("        * done crawling, found " + linksCount + " links.");
            this.success = true;
        } else {
            this.success = false;
        }
    }

    private Document downloadDocument ()
    {
        while (true){
            try {
                Connection.Response connection = Jsoup.connect(this.url)
                                .userAgent(this.USER_AGENT)
                                .header("Accept-Language", "en-US")
                                .followRedirects(true)
                                .timeout(6000)
                                .execute();

                //TODO check if this works (the .timeout().get() is new, was .get only)
                //this.htmlDocument =connection.timeout(6000).get();
                this.htmlDocument = connection.parse();

                if (this.htmlDocument.body()==null) //sometimes the page is in xml or any other format that is not html page, so it technicaly has no body
                    throw new IllegalArgumentException("Empty body!");
                if (this.htmlDocument.body().text().length() < 2)
                    throw new IllegalArgumentException("Empty body!");

                System.out.println("        * Downloaded document.");
                break;
            } catch (java.net.ConnectException | java.net.SocketTimeoutException ioe){
                System.out.println("        * Connection timed out, retrying ..");
                this.htmlDocument = null;
                this.success = false;
                break;
            } catch (IOException ioe) {
                System.out.println("        * Error in HTTP request, returning no document to crawler");
                if (this.url.charAt(this.url.length()-1)=='/') {
                    this.url = this.url.substring(0, this.url.length() -1);
                    System.out.println("        * Retrying at " + this.url);
                    continue;
                }
                this.htmlDocument = null;
                this.success = false;
                break;
            } catch (IllegalArgumentException ioe){
                System.out.println("        * Error in HTTP request, returning no document to crawler");
                this.htmlDocument = null;
                this.success = false;
                break;
            }
        }
        return this.htmlDocument;
    }

    private void findLinks()
    {
        if (this.htmlDocument != null){
            this.links = this.htmlDocument.select("a[href]");
            for (Element link : this.links) {

                // if external link
                // * we only work on HTTP protocol so we search for https or http in the link
                if (link.toString().contains("http:/") || link.toString().contains("https:/")){
                    ArrayList LINKS = new ArrayList();
                    String regex = "\\(?\\b(https?://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]";
                    Pattern p = Pattern.compile(regex);
                    Matcher m = p.matcher(link.toString());
                    while(m.find()) {
                        String urlStr = m.group();
                        if (urlStr.startsWith("(") && urlStr.endsWith(")"))
                        {
                            urlStr = urlStr.substring(1, urlStr.length() - 1);
                        }
                        LINKS.add(urlStr);
                    }
                    if (LINKS.size()!=0)
                        addLinkToList(LINKS.get(0).toString());
                }
                // if internal link
                else {
                    String newLink = link.toString();
                    try {
                        newLink = newLink.substring(newLink.indexOf("href") + 4);
                        newLink = newLink.substring(newLink.indexOf("\"") + 1);
                        newLink = newLink.substring(0, newLink.indexOf("\""));
                    } catch (StringIndexOutOfBoundsException e){
                        continue; //don't add this link into the list
                    }
                    if (newLink.startsWith("//"))
                    {
                        if (this.url.startsWith("https"))
                            newLink= "https:" + newLink;
                        else if (this.url.startsWith("http"))
                            newLink = "http:" + newLink;
                        addLinkToList(newLink);

                    }
                    else if (newLink.startsWith("/"))  {  //it is relative to the great parent URL
                        try {
                            String dummy =
                                    getParentURL(this.url) + newLink.substring(1,newLink.length());
                            addLinkToList(dummy);
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }
                    }
                    // it is relative to the referring page
                    else {
                        if (!this.url.endsWith("/") && !newLink.startsWith("/"))
                            this.url = this.url + "/";
                        String newUrl = this.url + newLink;
                        addLinkToList(newUrl);
                    }
                }
            }

            this.linksCount = this.httpLinks.size();
            System.out.println("        * found " + linksCount + " links.");
        }
        else {
            this.linksCount = 0;
            System.out.println("        * this spider wasn't successful crawling it's page");
        }
    }

    public void printLinks()
    {
        if (this.success) {
            for (String link : this.httpLinks) {
                System.out.println(link);
            }
        }
    }

    private void addLinkToList(String u){
        try {
            u = normalize(u);

            // CAN'T DO THAT / DON't DELETE THIS COMMENT
            // some links can't have a "/" at their end so we can;t add / to every link like this
            //if (!u.endsWith("/"))
            //   u = u + "/";
            if (u.charAt(u.length()-1) == '/')
                u = u.substring(0, u.length() - 1);
            httpLinks.add(u);
        } catch (MalformedURLException e) {
            //means it's not a valid url , or it's not a valid format
            // we don;t log the error as we get nearly 50,000 links so logging would be of no use
            //System.out.println(u);
            //e.printStackTrace();
        }
    }

    private boolean checkRobots()
    {
        String parentURL = null;
        try {
            parentURL = getParentURL(this.url);
        } catch (URISyntaxException e) {
            //e.printStackTrace();
            System.out.println("        * MalformedURL to " + parentURL + ", no changes to links list.");
            return false;
        }

        boolean allCrawlers = false;
        List<String> thisURLslashes = new ArrayList<>();

        // https://stackoverflow.com/questions/25731346/reading-robot-txt-with-jsoup-line-by-line

        try {
            System.out.println("        * Link :Trying to open robots.txt");
            URL url = new URL(parentURL + "robots.txt");
            URLConnection con = url.openConnection();
            con.setConnectTimeout(20 * 1000);
            con.setReadTimeout(20 * 1000);
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            //this inputstreamreader . openstream liha timeout 3 minutes , fa mtsta5demhash tani
            //BufferedReader in = new BufferedReader(new InputStreamReader(new URL(parentURL + "robots.txt").openStream()));
            String line = null;
            while ((line = in.readLine()) != null) {
                //System.out.println(line);
                ROBOTS.append(line);

                //extract the disallowed slashes of the ROBOTS file
                //line.substring("Disallow:".length() + 1, line.length())
                if (allCrawlers) {
                    if (line.contains("Disallow:") || line.contains("Allow:")) {
                        if (line.contains("/"))
                            thisURLslashes.add(line);
                    }
                }
                if (line.contains("User-agent: *")) { //first line
                    allCrawlers = true; //direct the behaviour
                } else if (line.contains("User-agent"))
                    allCrawlers = false; //the next time it sees User-agent redirect the behaviour
            }
            System.out.println("        * Downloaded robots.txt");

        } catch (MalformedURLException e) {
            System.out.println("        * MalformedURL to " + parentURL + ", no changes to links list.");
            //e.printStackTrace();
        } catch (java.net.ConnectException e){
            System.out.println("        * Connection timed out, trying to reconnect ...");
        } catch (java.io.FileNotFoundException e) {
            System.out.println("        * robots.txt was not found for " + parentURL + ", no changes to links list.");
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("        * Exception accessing " + parentURL + ", no changes to links list.");
        }


        if (this.url.contains("/localhost/")){
            return false;
        }

        boolean returnable = true;
        for (String slash : thisURLslashes) {
            String backup = slash;
            boolean aid;
            //if disallow, this loop incidence match will lead to a false
            if (slash.contains("Disallow:")) {
                aid = false;
                slash = slash.substring("Disallow:".length() + 1, slash.length());
            } else if (slash.contains("Allow:")){ //if allow, this loop incidence match will lead to a true
                aid = true;
                slash = slash.substring("Allow:".length() + 1, slash.length());
            } else {aid = true;} //protection


            //normal case
            if (url.contains(slash)){
                returnable = aid;
            }
            //special case
            else if (slash.equals("/") || slash.equals("/*") || slash.equals("*")){
                returnable = aid;
            }
            //case of * -> use regex to match
            else if (slash.contains("*")){
                String rms = ".*"; //regex matching string
                // hard case: /books?*zoom=1*    -> www.google.com/brebe2/books?bookname/zoom=12.html
                while (slash.contains("*"))
                {
                    //5adna el goz2 da fi string
                    rms = rms + slash.substring(0,slash.indexOf("*"));
                    //we shelnah mn el slash
                    slash = slash.substring(slash.indexOf("*") + 1, slash.length());
                    //we a7ot el .* fl string
                    rms += ".*";
                }
                // lghayet ma yb2a mfish slashes fa a7ot el ba2i kolo
                rms+= slash;

                if (this.url.matches(rms))
                    returnable = aid;
            }
        }

        if (!returnable) System.out.println("        * Disallowed by robots.txt, spider was unsuccessful.");
        return returnable;
    }

    private void fillBundleData()
    {
        //ID
        //data.id  ??

        //URL
        data.setUrl(this.url);

        //ROBOTS & DESCRIPTION  https://stackoverflow.com/questions/37591685/parsing-the-html-meta-tag-with-jsoup-library
        Elements metaTags = htmlDocument.getElementsByTag("meta");
        for (Element metaTag : metaTags){
            String content = metaTag.attr("content");
            String name = metaTag.attr("name");
            if ("robots".equals(name.toLowerCase()))
                data.setRobots(content);
            else if ("description".equals (name.toLowerCase()))
                data.setDescription(content);
        }

        //CHILDREN -- must be after ROBOTS as robots edits the httpLinks list
        data.setChild(httpLinks);

        //PARENTS
        //data.setParent("");

        //TITLE
        if (htmlDocument.title() != null)
            data.setTitle(htmlDocument.title());

        //HEADER
        if (htmlDocument.head() != null)
            data.setHeader(htmlDocument.head().text());

        //BODY -- customized
        if (htmlDocument.body() != null) {
            if (longestLenght(htmlDocument.body().text()) > 50)
            {
                Elements ll = htmlDocument.body().select("tr");
                for (Element e : ll)
                    e.replaceWith(new TextNode(" ", ""));

                data.setBody(getBody().replaceAll("[^a-zA-Z0-9' ]"," "));

            } else  // normal case
                data.setBody(getBody().replaceAll("[^a-zA-Z0-9' ]"," "));
        } else {
            System.out.println("        * Error getting html document contains no body. body set to empty string");
            data.setBody(" ");
        }

        //HASH
        if (htmlDocument.body() != null) {
            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                md.update(getBody().replaceAll("[^a-zA-Z0-9' ]"," ").getBytes());
                byte[] digest = md.digest();
                String myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
                System.out.println("        * Hash: " + myHash);
                //TODO fill the bundle with hash
                data.setHash(myHash);

            } catch (NoSuchAlgorithmException e) {
                System.out.println("        * Error hashing html body. hash set to empty string");
                data.setHash(" ");
                e.printStackTrace();
            }
        } else {
            System.out.println("        * Error hashing html body as document contains no body. hash set to empty string");
            data.setHash(" ");
        }


    }

    private String getParentURL(String url) throws URISyntaxException
    {
        int currLenght , prvLenght = 0;
        URI uri = new URI(url);
        URI parent = uri.getPath().endsWith("/") ? uri.resolve("..") : uri.resolve(".");
        while (true){
            prvLenght = parent.toString().length();
            parent = parent.getPath().endsWith("/") ? parent.resolve("..") : parent.resolve(".");
            currLenght = parent.toString().length();
            String tmpText = parent.toString().substring(parent.toString().length() - 2, parent.toString().length() );
            if (Objects.equals(tmpText, ".."))
                return (parent.toString().substring(0,parent.toString().length() - 2));
            else if (currLenght == prvLenght)
                return (parent.toString() + "/");
        }
    }

    public Bundle getData()
    {
        return this.data;

    }

    private int longestLenght(String s){
        String[] word=s.split(" ");
        String rts=" ";
        for(int i=0;i<word.length;i++){
            if(word[i].length()>=rts.length()){
                rts=word[i];
            }
        }
        return rts.length();
    }


    //external links only
    private static String normalize(String url) throws MalformedURLException {

        if (url.endsWith(".pdf") || url.endsWith(".jpg") || url.endsWith(".jpeg") || url.endsWith(".png") || url.endsWith(".mp3"))
            throw new MalformedURLException(" ");

        URL u = new URL(url);
        String proto = u.getProtocol().toLowerCase();
        String host = u.getHost().toLowerCase();
        int port = u.getPort();

        if (port != -1) {

            switch (port) {
                case 21:
                    if (proto.equals("ftp")) {
                        port = -1;
                    }
                    break;
                case 80:
                    if (proto.equals("http")) {
                        port = -1;
                    }
                    break;
                case 443:
                    if (proto.equals("https")) {
                        port = -1;
                    }
                    break;
            }
        }


        URL _nu;
        if (port == -1) {
            _nu = new URL(proto, host, u.getFile());
        } else {
            _nu = new URL(proto, host, u.getFile());
        }
        return _nu.toString();
    }

    private String getBody(){

        ArrayList<Element> elementsArr = new ArrayList<>();

        //use owntext() instead of text() to avoid getting text of children and avoid duplicate/trashy text
        elementsArr.addAll(htmlDocument.select("p"));
        elementsArr.addAll(htmlDocument.select("h1"));
        elementsArr.addAll(htmlDocument.select("h2"));
        elementsArr.addAll(htmlDocument.select("h3"));
        elementsArr.addAll(htmlDocument.select("h4"));
        elementsArr.addAll(htmlDocument.select("h5"));
        elementsArr.addAll(htmlDocument.select("h6"));
        elementsArr.addAll(htmlDocument.select("span"));
        elementsArr.addAll(htmlDocument.select("strong"));
        elementsArr.addAll(htmlDocument.select("a"));
        elementsArr.addAll(htmlDocument.select("title"));
        elementsArr.addAll(htmlDocument.select("div"));

        Elements bodySymbol = htmlDocument.select("symbol");

        String bodyText = "";
        String symbolText = "";
        for (int i = 0;i< bodySymbol.size(); i++){
            String d = bodySymbol.get(i).ownText();
            char[] dc = d.toCharArray();
            boolean take = true;
            for (int j = 0; j< d.length(); j++){
                if ( dc[j] == '<') { take = false; continue;}
                else if ( dc[j] == '>') { take = true;  continue;}
                else if (take) symbolText+= dc[j];
            }
        }
        //StringEscapeUtils.unescapeHtml4(

        for (int i = 0; i < elementsArr.size(); i++){
            if (elementsArr.get(i).hasText() && elementsArr.get(i).ownText().length() > 1)
                //System.out.println(elementsArr.get(i).ownText());
                bodyText+=(elementsArr.get(i).ownText() + "\n");
        }

        bodyText = bodyText.replaceAll("\\s+", " ");
        //System.out.println(bodyText);

        return bodyText;
    }


}


