package cimain;

import com.mongodb.*;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;

import java.io.PrintWriter;


public class Utilities {

    public static void initialDatabase(){

        MongoDatabase mongoDB = new MongoDatabase();
        DB database = mongoDB.connectMongo();

        for (String string : database.getCollectionNames()) {

            DBCollection collection = database.getCollection(string);

            try {
                collection.drop();
            } catch (MongoCommandException u){

            }

        }

        database.createCollection("collection1",new BasicDBObject());
        database.createCollection("collection2",new BasicDBObject());
        database.createCollection("collection3",new BasicDBObject());
        database.createCollection("collection4",new BasicDBObject());
        database.createCollection("collection5",new BasicDBObject());
        database.createCollection("sessions",new BasicDBObject());
        database.createCollection("subscriptions",new BasicDBObject());

        DBCollection col3 = database.getCollection("collection3");
        col3.createIndex("data");


        DBCollection col1 = database.getCollection("collection1");
        col1.createIndex(new BasicDBObject("url",1),"u",true);
        col1.createIndex("indexed");

        DBCollection col2 = database.getCollection("collection2");
        col2.createIndex("keyword");
        col2.createIndex("urls");

        DBCollection users = database.getCollection("sessions");
        col2.createIndex("sessionID");

        DBCollection subs = database.getCollection("subscriptions");
        col2.createIndex("url");

        BasicDBObject ob = new BasicDBObject();
        ob.put("stoppingCritria",0);
        ob.put("crawelVersion",1);
        ob.put("priorityState",0);
        database.getCollection("collection4").insert(ob);

        try {
            PrintWriter writer = new PrintWriter("input.txt");
            writer.print("");
            writer.close();
        }catch (Exception e){

        }

    }

    public String removeDotsFromUrl(String r){
        return (new String(r.replace(".", "[$$]")));
    }

    public String getDotsBackToUrl(String r){
        return (new String(r.replace("[$$]", ".")));
    }


    public static void main (String [] args){
        initialDatabase();

    }

}
