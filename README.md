# APT project SearchEngine

## Api
    install apache tomcat7 (apt-get install tomcat7)
- IMPORTANT: [tomcat7 usage tutorial](http://www.vogella.com/tutorials/ApacheTomcat/article.html)
- [create .war file and deploy to tomcat](http://www.geekabyte.io/2015/07/how-to-create-war-files-with-intellij.html)


#### ENDPOINTs (/api)
###### - Search

    GET ( /search?q=QUERY_HERE&page=PAGE_NUMBER )(Header -> identifier:SESSION_ID)
###### - AutoComplete

    GET ( /autocomplete?w=SUBWORD_HERE)
###### - Get Notifications

    GET ( /getnotifications) (Header -> identifier:SESSION_ID)
###### - Subscribe & Mard notification asRead

    GET ( /subscribe?l=LINK_HERE) (Header -> identifier:SESSION_ID)
###### - unSubscribe

    GET ( /unsubscribe?l=LINK_HERE) (Header -> identifier:SESSION_ID)




## Phase 1
- WebCrawler
- Indexer

### Stacks used
- JAVA (mainapi logic)
- MongoDB (data storage)

### Libraries included
- Mongodb-Java-Driver
- Jsoup
- Snowball stemmer

### To Clone and Run Correctly
 - Clone the project
 - Open IntelliJ Idea (don't 'Open' the project)
 - 'Import' the project folder and go through the steps with default parameters
 - After creating the project, go through all the files to check for RED marked errors in the imports above, and press alt+enter for every error then "add library 'Maven ..' to class path"
 - Run Utilities then Main

### To Run
#####1- first run Utilities.mainapi()
#####2- in IDEA ( to run 2 mainapi() )
- menu Run -> Edit configrations add new compound from green(+) icon  
- add mainapi,Indexer mains from green(+) on the right  
- name compound with ay7aga  
- save then run el compound :)  


 
`  *** USE IntelliJ IDEA with MAVEN***  `