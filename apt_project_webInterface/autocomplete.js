

///////////////////////////////////////////////////////////////////////
let server = "http://91bceed3.ngrok.io/";
let local = 'http://localhost:3000/';

function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      let session_id = localStorage.getItem("session_id");
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      //this.parentNode.parentNode.appendChild(a);
      document.getElementById("listlistlist").appendChild(a);

      //custom code
      let words = document.getElementById("lst-ib").value;
      words = words.split(" ");
      let word = words[words.length - 1].toLowerCase();
      if (word.length)
      //get the array here
      $.ajax({
        url: server + 'api/autocomplete?w=' + word,
        dataType: 'JSON',
        type: 'GET',
        //this sends a OPTIONS request before sending my request
        //beforeSend: function(xhr){
        //  xhr.setRequestHeader('identifier', session_id);
        //},
        success: function (result) {
          if (result.isOk == false)
            error.log(result.message);
          else {
            let arr_autocomplete;
            if (result.list)
            arr_autocomplete = result.list; //array

            let t = false;
            for (let kk = 0; kk < arr_autocomplete.length; kk++)
              if (arr_autocomplete[kk].length != 1 ) t = true;
            if (!t){
              console.log(arr_autocomplete);
              arr_autocomplete = arr_autocomplete.join("");
            }
            if (result.list)
            //append the links list
            for (let i = 0; i < arr_autocomplete.length; i++){
              //use link as the data
              //create a DIV element for each matching element:
              let link = arr_autocomplete[i];
              b = document.createElement("DIV");
              b.innerHTML = link;
              //insert a input field that will hold the current array item's value:
              b.innerHTML += "<input type='hidden' value='" + link + "'>";
              //execute a function when someone clicks on the item value (DIV element):
                  b.addEventListener("click", function(e) {
                  //insert the value for the autocomplete text field:
                  words[words.length - 1] = this.getElementsByTagName("input")[0].value;
                  inp.value = words.join(" ");
                  //close the list of autocompleted values,
                  //(or any other open lists of autocompleted values:
                  closeAllLists();
              });
              a.appendChild(b);

            }
          }
        },
        error: function(err){
          console.error("Error fetching autocomplete from server (Reason: can't connect!)");
        }
      });
  });

  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        let go = false;
        if (document.getElementById("listlistlist").children[0]){
          if (!document.getElementById("listlistlist").children[0].children.length)
            go = true;
        } else go = true;
        if (document.getElementById("listlistlist").children.length && currentFocus == -1)
          go = true;
        if (!go)
          e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
      x[i].parentNode.removeChild(x[i]);
    }
  }
}
/*execute a function when someone clicks in the document:*/
document.addEventListener("click", function (e) {
    closeAllLists(e.target);
});
}
