
/*
TODO here:
* on start: check if there was a web session saved for our search engine in local storage
  and request one if there wasn't (using /api/create-session)
* using the session key request my notifications (using /api/get-links)
* update view and show notifications (response of the second request)
*/
var session_id;
window.onload = function () {

  autocomplete(document.getElementById("lst-ib"),  [])


  if (typeof(Storage) !== "undefined") {
    // Store
    //localStorage.setItem("5ara", "kelab");
    // Retrieve
    //localStorage.getItem("lastname")  //empty is null
    // Remove
    //localStorage.removeItem("5ara");
    session_id = localStorage.getItem("session_id");
    if (session_id != null){
      // there is a saved session
      get_links(session_id);
    } else {
      // there isn't a saved session, create one now
      create_session();
    }


  } else {
      console.log("Sorry! No Web Storage support..");
  }

}


function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 28; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

function create_session(){
  localStorage.setItem("session_id", makeid());
}

function get_links(session_id){
  axios.get( server + 'api/getnotifications',
  {
    headers: {
                'identifier': session_id
              }
  }).then(response => {
      // If request is good...
      console.log(response.data.message);
      // append the place for the list
      let str = "<div class='card' style='text-align:center; padding-top: 90px;'>" +
      " <h4><b>Links that may interest you again.</b></h4>";

      if (response.data.list){
        if (!response.data.list[0].length)
        for (let i = 0; i < response.data.list.length; i++){
          str += "<a target='_blank' onclick='notificationOnClick(this);' href= '" + response.data.list[i] +  "'>" + response.data.list[i] + "</a><br>";
        }
        else str += "<a target='_blank' onclick='notificationOnClick(this);' href= '" + response.data.list +  "'>" + response.data.list + "</a><br>";
      }

      // end the list DOM object
      str+= "</div>"
      $( "#viewport" ).append(str);
    }).catch((error) => {
      console.log(error);
      console.error("Error fetching notifications from server (Reason: can't connect!)");
    });
}

function notificationOnClick(DOMobj){

  //    /unsubscribe?l=LINK_HERE
  axios.get( server + 'api/subscribe?l=' + DOMobj,
  {
    headers: {
                'identifier': session_id
              }
  }).then(response => {
      // If request is good...
      console.log(response.data.message);
    }).catch((error) => {
      //console.log(error);
      console.error("Error unsubscribing to link (Reason: can't connect!)");
    });
}
