var session_id;
//var server="http://5c10c102.ngrok.io/";
var query =getParameterByName("q",document.URL);
window.onload = function () {

  
  autocomplete(document.getElementById("lst-ib"),  [])

  
  var page =0;
  document.getElementById("lst-ib").value = query;
  if (typeof(Storage) !== "undefined") {
    session_id = localStorage.getItem("session_id");
  }

  sendReq(query,page);
  

  
}


function getParameterByName(name, url) {
  if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
  results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function sendReq(query,pageNum) {


  $.ajax({
    url: server + 'api/search?q=' + query + '&page=' + pageNum,
    dataType: 'JSON',
    type: 'GET',
    beforeSend: function(xhr){
      xhr.setRequestHeader('identifier', session_id);
    },
    success: function (result) {
      console.log(result);

      $( "#showData" ).html("");
      
      if (result.results.length){

        for (var i = 0; i < result.results.length; i++) {
          var stringData="<h3 class='r' id='buttonClass'><a ";
          var url = result.results[i].link;
          stringData+="href='"+url+"' onmousedown='return;'>";
          stringData+=result.results[i].title+" </a></h3><div class='s'><div>  <div class='f hJND5c TbwUpd' style='white-space:nowrap'><cite class='iUh30' id='url'>";
          stringData+=url+"</cite></div><span class='st'>";
          stringData+=result.results[i].subBody+"</span></div>";
          var subscribe=result.results[i].subscribed;
          var buttons;
          if(subscribe=="false"){
            buttons ="<input id='"+url+ "' class='subscribeBtn' type='button' value='Subscribe'></input>" + "</div>";
          }
          else {
            buttons ="<input id='"+url+ "' class='subscribeBtn' type='button' value='CancelSubscribe'></input>" +"</div>";
          }
          stringData+=buttons;
          stringData+="<br>";

          $( "#showData" ).append(stringData);
        }

        $(".subscribeBtn").click(function( event ) {
          if (event.target.value=="Subscribe"){
            $.ajax({
              url: server+'api/subscribe?l='+event.target.id,
              dataType: 'JSON',
              type: 'GET',
              beforeSend: function(xhr){
                xhr.setRequestHeader('identifier', session_id);
              },
              success: function (result) {
                if (result.isOk == false)
                  error.log(result.message);
                else {
                  console.log(event.target.id);
                  event.target.value = "CancelSubscribe";
                }
              },
              error: function(err){
                console.error("Error fetching links from server (Reason: can't connect!)");
              }
            });
          } else {

            $.ajax({
              url: server+'api/unsubscribe?l='+event.target.id,
              dataType: 'JSON',
                  type: 'GET',
              beforeSend: function(xhr){
                xhr.setRequestHeader('identifier', session_id);
              },
              success: function (result) {
                
                if (result.isOk == false)
                  error.log(result.message);
                else {
                  event.target.value="Subscribe";
                }
              },
              error: function(err){
                console.error("Error fetching links from server (Reason: can't connect!)");
              }
            });
          }
        });
         
        $('#pagination-here').bootpag({
            total: result.pageCount,          // total pages
            page: (parseInt(result.pageNumber) +1),            // default page
            maxVisible: 10,     // visible pagination
         
        }).on("page", function(event, num){
            sendReq(query,num-1); 
        });
      }
    },
    error: function(err){
      console.error("Error fetching links from server (Reason: can't connect!)");
    }
  });
}


var table = document.getElementById("nav");
if (table != null) {
    for (var i = 0; i < table.rows.length; i++) {
        for (var j = 0; j < table.rows[i].cells.length; j++)
        table.rows[i].cells[j].onclick = function () {
            tableText(this);
        };
    }
}

function tableText(tableCell) {
    alert(tableCell.innerHTML);
}
